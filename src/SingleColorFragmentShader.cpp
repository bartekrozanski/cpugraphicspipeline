#include "SingleColorFragmentShader.h"

void SingleColorFragmentShader::RunShader(FragmentStream& fragmentStream)
{
	for (Fragment& fragment : fragmentStream)
	{
		fragment.color = color;
	}
}

void SingleColorFragmentShader::SetShadingColor(glm::vec3 color)
{
	this->color = color;
}
