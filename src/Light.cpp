#include "Light.h"


Light::Light( Model const*model,
	 MyTexture const*texture,
	glm::vec3 lightColor):
	Instance(model, texture),
	lightColor(lightColor)
{
}


glm::vec3 Light::GetLightColor() const
{
	return lightColor;
}
