#include "ModelManager.h"



ModelManager::ModelManager()
{
}


ModelManager::~ModelManager()
{
}

void ModelManager::AddModel(const std::string & name, const Model & model)
{
	models.insert({ name, model });
}

const Model* ModelManager::GetModel(const std::string & name)
{
	return &models[name];
}
