#include "SFMLWindow.h"

SFMLWindow::SFMLWindow(int width, int height) :
	Window(width, height),
	window(sf::VideoMode(width, height),"SFMLWindow")
{
}

SFMLWindow::~SFMLWindow()
{
}

void SFMLWindow::Display(const Framebuffer & framebuffer)
{
	sf::Image image;
	image.create(width, height, (sf::Uint8*) framebuffer.GetBuffer());
	sf::Texture texture;
	texture.loadFromImage(image);



	//while (window.isOpen())
	//{
	//	sf::Event event;
	//	while (window.pollEvent(event))
	//	{
	//		if (event.type == sf::Event::Closed)
	//			window.close();
	//	}

		window.clear();
		window.draw(sf::Sprite(texture));

		window.display();
	//}
}

bool SFMLWindow::PollEvent(Event & event)
{
	sf::Event e;

	while (window.pollEvent(e))
	{
		switch (e.type)
		{
		case sf::Event::KeyPressed:
		case sf::Event::KeyReleased:
			switch (e.key.code)
			{
			case sf::Keyboard::A:
				event.keyEvent.key = Key::A;
				break;
			case sf::Keyboard::D:
				event.keyEvent.key = Key::D;
				break;
			case sf::Keyboard::S:
				event.keyEvent.key = Key::S;
				break;
			case sf::Keyboard::W:
				event.keyEvent.key = Key::W;
				break;
			case sf::Keyboard::F:
				event.keyEvent.key = Key::F;
				break;
			case sf::Keyboard::Up:
				event.keyEvent.key = Key::ArrowUp;
				break;
			case sf::Keyboard::Down:
				event.keyEvent.key = Key::ArrowDown;
				break;
			case sf::Keyboard::Left:
				event.keyEvent.key = Key::ArrowLeft;
				break;
			case sf::Keyboard::Right:
				event.keyEvent.key = Key::ArrowRight;
				break;
			case sf::Keyboard::J:
				event.keyEvent.key = Key::J;
				break;
			case sf::Keyboard::L:
				event.keyEvent.key = Key::L;
				break;
			case sf::Keyboard::I:
				event.keyEvent.key = Key::I;
				break;
			case sf::Keyboard::K:
				event.keyEvent.key = Key::K;
				break;
			case sf::Keyboard::U:
				event.keyEvent.key = Key::U;
				break;
			case sf::Keyboard::O:
				event.keyEvent.key = Key::O;
				break;
			case sf::Keyboard::LBracket:
				event.keyEvent.key = Key::LeftSquareBracket;
				break;
			case sf::Keyboard::RBracket:
				event.keyEvent.key = Key::RightSquareBracket;
				break;
			case sf::Keyboard::Escape:
				event.keyEvent.key = Key::Esc;
				break;
			case sf::Keyboard::BackSpace:
				event.keyEvent.key = Key::Backspace;
				break;
			case sf::Keyboard::Num1:
				event.keyEvent.key = Key::Num1;
				break;
			case sf::Keyboard::Num2:
				event.keyEvent.key = Key::Num2;
				break;
			case sf::Keyboard::Num3:
				event.keyEvent.key = Key::Num3;
				break;
			case sf::Keyboard::Num4:
				event.keyEvent.key = Key::Num4;
				break;
			case sf::Keyboard::Num5:
				event.keyEvent.key = Key::Num5;
				break;
			case sf::Keyboard::Num6:
				event.keyEvent.key = Key::Num6;
				break;
			case sf::Keyboard::Num7:
				event.keyEvent.key = Key::Num7;
				break;
			case sf::Keyboard::Num8:
				event.keyEvent.key = Key::Num8;
				break;
			case sf::Keyboard::Num9:
				event.keyEvent.key = Key::Num9;
				break;
			case sf::Keyboard::Num0:
				event.keyEvent.key = Key::Num0;
				break;
			default:
				return false;
			}
			if (e.type == sf::Event::KeyPressed)
			{
				event.type = EventType::KeyPressed;
			}
			else
			{
				event.type = EventType::KeyReleased;
			}
			return true;
		default:
			break;
		}
	}
	return false;
}

void SFMLWindow::DisplayInTitle(double val)
{
	window.setTitle(std::to_string(val));
}
