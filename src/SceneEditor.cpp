#include "SceneEditor.h"
#include "App.h"

SceneEditor::SceneEditor(App& app) :
	app(app)
{
	selection.selectionType = SelectionType::None;
}

void SceneEditor::TranslateSelected(float x, float y, float z)
{
	if (IsSelected())
	{
		GetSelectedObject().SetRelativeTranslation(x, y, z);
	}
}

void SceneEditor::SelectNext()
{
	selection.index++;
	switch (selection.selectionType)
	{
	case SelectionType::None:
		SelectFirstObject(SelectionType::Instance);
		break;
	case SelectionType::Instance:
		if (app.instances.size() == selection.index)
			SelectFirstObject(SelectionType::Light);
		break;
	case SelectionType::Light:
		if (app.lights.size() == selection.index)
			SelectFirstObject(SelectionType::Camera);
		break;
	case SelectionType::Camera:
		if (app.cameras.size() == selection.index)
			SelectFirstObject(SelectionType::None);
		break;
	default:
		break;
	}
}

void SceneEditor::SelectPrevious()
{
	selection.index--;
	switch (selection.selectionType)
	{
	case SelectionType::None:
		SelectLastObject(SelectionType::Camera);
		break;
	case SelectionType::Instance:
		if (selection.index < 0)
			SelectLastObject(SelectionType::None);
		break;
	case SelectionType::Light:
		if (selection.index < 0)
			SelectFirstObject(SelectionType::Instance);
		break;
	case SelectionType::Camera:
		if (selection.index < 0)
			SelectLastObject(SelectionType::Light);
		break;
	default:
		break;
	}
}


Instance& SceneEditor::GetSelectedObject()
{
	switch (selection.selectionType)
	{
	case SelectionType::Instance:
		return app.instances[selection.index];
	case SelectionType::Light:
		return app.lights[selection.index];
	case SelectionType::Camera:
		return app.cameras[selection.index];
	default:
		throw std::exception("No object is selected");
	}
}

void SceneEditor::Unselect()
{
	selection.selectionType = SelectionType::None;
}

bool SceneEditor::IsSelected()
{
	return selection.selectionType != SelectionType::None;
}

void SceneEditor::SpawnSphere()
{
	app.InstantiateModel("sphere", "solidRed", glm::vec3(0.0f), 0.0f, glm::vec3(1.0f), glm::vec3(1.0f));
	SelectLastObject(SelectionType::Instance);
}

void SceneEditor::SpawnCube()
{
	app.InstantiateModel("cube", "dirt", glm::vec3(0.0f), 0.0f, glm::vec3(1.0f), glm::vec3(1.0f));
	SelectLastObject(SelectionType::Instance);
}

void SceneEditor::SpawnLight()
{
	app.InstantiateLight("cube", "light", glm::vec3(0.0f), 0.0f, glm::vec3(1.0f), glm::vec3(1.0f), glm::vec3(1.0));
	SelectLastObject(SelectionType::Light);
}

void SceneEditor::DeleteSelectedObject()
{
	switch (selection.selectionType)
	{
	case SelectionType::None:
		break;
	case SelectionType::Instance:
		app.instances.erase(app.instances.begin() + selection.index);
		SelectPrevious();
		break;
	case SelectionType::Light:
		app.lights.erase(app.lights.begin() + selection.index);
		SelectPrevious();
		break;
	case SelectionType::Camera:
		app.cameras.erase(app.cameras.begin() + selection.index);
		SelectPrevious();
		break;
	default:
		break;
	}
}

void SceneEditor::SelectFirstObject(SelectionType selectionType)
{
	selection.selectionType = selectionType;
	selection.index = 0;
	switch (selectionType)
	{
	case SelectionType::None:
		break;
	case SelectionType::Instance:
		if (app.instances.size() == 0)
			SelectFirstObject(SelectionType::Light);
		break;
	case SelectionType::Light:
		if (app.lights.size() == 0)
			SelectFirstObject(SelectionType::Camera);
		break;
	case SelectionType::Camera:
		if (app.cameras.size() == 0)
			SelectFirstObject(SelectionType::None);
		break;
	default:
		break;
	}
}

void SceneEditor::SelectLastObject(SelectionType selectionType)
{
	selection.selectionType = selectionType;
	switch (selectionType)
	{
	case SelectionType::None:
		break;
	case SelectionType::Instance:
		if (app.instances.size() == 0)
			SelectLastObject(SelectionType::None);
		else
		{
			selection.index = app.instances.size() - 1;
		}
		break;
	case SelectionType::Light:
		if (app.lights.size() == 0)
			SelectLastObject(SelectionType::Instance);
		else
		{
			selection.index = app.lights.size() - 1;
		}
		break;
	case SelectionType::Camera:
		if (app.cameras.size() == 0)
			SelectLastObject(SelectionType::Light);
		else
		{
			selection.index = app.cameras.size()-1;
		}
		break;
	default:
		break;
	}
}
