#include "Camera.h"

Camera::Camera( Model const* model,
	 MyTexture const *texture,
	float fov,
	float aspectRatio,
	float nearPlane,
	float farPlane):
	Instance(model, texture),
	fov(fov),
	aspectRatio(aspectRatio),
	nearPlane(nearPlane),
	farPlane(farPlane),
	projectionMatrix(glm::perspective(
		glm::radians(fov),
		aspectRatio,
		nearPlane,
		farPlane
	))
{
}


glm::mat4 Camera::GetViewMatrix()
{
	return glm::inverse(GetWorldMatrix());
}

const glm::mat4& Camera::GetProjectionMatrix() const
{
	return projectionMatrix;
}

glm::vec3 Camera::GetRight() const
{
	return glm::vec3(rotateMatrix[0]);
}

glm::vec3 Camera::GetDirection() const
{
	return glm::vec3(rotateMatrix[2]);
}
