#include "Clock.h"



Clock::Clock()
{
}


Clock::~Clock()
{
}

void Clock::Start()
{
	t0 = std::chrono::high_resolution_clock::now();
}

double Clock::Stop()
{
	t1 = std::chrono::high_resolution_clock::now();
	std::chrono::duration<double, std::milli> ret = t1 - t0;
	return ret.count();
}

void Clock::Sleep(double miliseconds)
{
	auto duration = std::chrono::duration<double, std::milli>(miliseconds);
	std::this_thread::sleep_for(duration);
}
