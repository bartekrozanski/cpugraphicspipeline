#include "LightFragmentShader.h"


LightFragmentShader::~LightFragmentShader()
{
}

void LightFragmentShader::RunShader(FragmentStream & fragmentStream)
{
	for (Fragment& fragment : fragmentStream)
	{
		fragment.color = lightColor * texture->GetColor(fragment.texPos);
	}
}

void LightFragmentShader::SetShadingColor(glm::vec3 color)
{
	lightColor = color;
}

void LightFragmentShader::SetTexture(MyTexture const * texture)
{
	this->texture = texture;
}
