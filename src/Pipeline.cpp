#include "Pipeline.h"



Pipeline::Pipeline(Rasterizer & rasterizer, Framebuffer & framebuffer, ZBuffer& zBuffer, Clipper& clipper):
	rasterizer(rasterizer),
	framebuffer(framebuffer),
	zBuffer(zBuffer),
	clipper(clipper)
{
}

Pipeline::~Pipeline()
{
}

void Pipeline::Draw(const Model& model,
	VertexShader* vertexShader,
	FragmentShader* fragmentShader,
	Primitive interpretation,
	const Viewport& viewport)
{
	VertexStream vertexStream(model.first);
	Indices indices(model.second);

	//Vertex Shader
	vertexShader->RunShader(vertexStream);

	//Primitive Assebly
	vertexStream = AssemblyPrimitives(vertexStream,indices,  interpretation);

	//Clipping
	//near plane
	clipper.SetClippingPlaneNormal(glm::vec4(0.0f, 0.0f, 1.0f, 1.0f));
	clipper.SetClippingPlaneOffset(-0.1f);
	vertexStream = clipper.ClipTriangles(vertexStream);
	//far plane
	clipper.SetClippingPlaneNormal(glm::vec4(0.0f, 0.0f, -1.0f, -1.0f));
	clipper.SetClippingPlaneOffset(100.0f);
	vertexStream = clipper.ClipTriangles(vertexStream);
	//left plane
	clipper.SetClippingPlaneNormal(glm::vec4(1.0f, 0.0f, 0.0f, 1.0f));
	clipper.SetClippingPlaneOffset(-0.05f);
	vertexStream = clipper.ClipTriangles(vertexStream);
	//right plane
	clipper.SetClippingPlaneNormal(glm::vec4(-1.0f, 0.0f, 0.0f, 1.0f));
	clipper.SetClippingPlaneOffset(-0.05f);
	vertexStream = clipper.ClipTriangles(vertexStream);
	//bottom plane
	clipper.SetClippingPlaneNormal(glm::vec4(0.0f, 1.0f, 0.0f, 1.0f));
	clipper.SetClippingPlaneOffset(-0.1f);
	vertexStream = clipper.ClipTriangles(vertexStream);
	//top plane
	clipper.SetClippingPlaneNormal(glm::vec4(0.0f, -1.0f, 0.0f, 1.0f));
	clipper.SetClippingPlaneOffset(-0.1f);
	vertexStream = clipper.ClipTriangles(vertexStream);

	//to NDC
	for (auto it = vertexStream.begin(); it != vertexStream.end(); ++it)
	{
		it->position = it->position / it->position.w;
	}

	//NDC -> viewport transform
	for (auto it = vertexStream.begin(); it != vertexStream.end(); ++it)
	{
		it->position = viewport.GetViewportTransform() * it->position;
	}

	//back culling
	for (int i = 0; i < vertexStream.size();)
	{
		if (FacesBack(vertexStream[i], vertexStream[i + 1], vertexStream[i + 2]))
		{
			vertexStream.erase(vertexStream.begin() + i, vertexStream.begin() + i + 3);
		}
		else
		{
			i += 3;
		}
	}

	//Rasterization
	auto fragmentStream = rasterizer.RasterizeVertexStream(vertexStream, interpretation);

	//FragmentShader
	fragmentShader->RunShader(fragmentStream);

	//Fragments -> framebuffer (with z-buffer testing)
	for (Fragment& fragment: fragmentStream)
	{
		if (zBuffer.Visible((int)(fragment.position.x), (int)(fragment.position.y), (fragment.position.z)))
		{
			framebuffer.SetColor((int)fragment.position.x,
				(int)fragment.position.y,
				(Byte)(fragment.color.r * 255),
				(Byte)(fragment.color.g*255),
				(Byte)(fragment.color.b*255));
		}
	}
}

void Pipeline::Clear()
{
	framebuffer.Clear();
	zBuffer.Clear();
}


VertexStream Pipeline::AssemblyPrimitives(const VertexStream & vertexStream, const Indices & indices, Primitive interpretation)
{
	switch (interpretation)
	{
	case Primitive::FRAME:
	case Primitive::TRIANGLE:
		return AssemblyTriangles(vertexStream, indices);
		break;
	default:
		break;
	}
}

VertexStream Pipeline::AssemblyTriangles(const VertexStream& vertexStream, const Indices& indices)
{
	if (indices.empty()) return vertexStream;
	VertexStream newStream;
	for (int i = 0; i < indices.size(); ++i)
	{
		newStream.push_back(vertexStream[indices[i]]);
	}
	return newStream;
}

VertexStream Pipeline::AssemblyFrame(const VertexStream & vertexStream, const Indices & indices)
{
	VertexStream newStream;
	if (indices.empty())
	{
		for (int i = 0; i < vertexStream.size(); i+=3)
		{
			newStream.push_back(vertexStream[i]);
			newStream.push_back(vertexStream[i+1]);
			newStream.push_back(vertexStream[i + 1]);
			newStream.push_back(vertexStream[i + 2]);
			newStream.push_back(vertexStream[i + 2]);
			newStream.push_back(vertexStream[i]);
		}
	}
	else
	{
		for (int i = 0; i < indices.size(); i += 3)
		{
			newStream.push_back(vertexStream[indices[i]]);
			newStream.push_back(vertexStream[indices[i + 1]]);
			newStream.push_back(vertexStream[indices[i + 1]]);
			newStream.push_back(vertexStream[indices[i + 2]]);
			newStream.push_back(vertexStream[indices[i + 2]]);
			newStream.push_back(vertexStream[indices[i]]);
		}
	}
	return newStream;
}


bool Pipeline::FacesBack(const Vertex & v0, const Vertex & v1, const Vertex & v2)
{

	return (v1.position.x - v0.position.x)*(v2.position.y - v0.position.y) -
		(v1.position.y - v0.position.y)*(v2.position.x - v0.position.x)>=0;
}
