#include "ZBuffer.h"


ZBuffer::ZBuffer(int width, int height):
	width(width),
	height(height)
{
	buffer = new float[width*height];
}

ZBuffer::~ZBuffer()
{
	delete[] buffer;
}

void ZBuffer::Clear()
{
	for (int i = 0; i < width*height; ++i)
	{
		buffer[i] = std::numeric_limits<float>::max();
	}
}

bool ZBuffer::Visible(int x, int y, float z)
{
	if (buffer[y*width + x] > z)
	{
		buffer[y*width + x] = z;
		return true;
	}
	return false;
	
}
