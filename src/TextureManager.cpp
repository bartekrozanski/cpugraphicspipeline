#include "TextureManager.h"

const MyTexture* TextureManager::GetTexture(const std::string& name)
{
	return &textures[name];
}

void TextureManager::LoadTexture(const std::string& path, const std::string& name)
{
	textures.insert({ name, MyTexture(path) });
}
