#include "MyTexture.h"

MyTexture::MyTexture(const std::string& path)
{
	image.loadFromFile(path);
	maxCoords = glm::vec2(image.getSize().x - 1, image.getSize().y - 1);
}

glm::vec3 MyTexture::GetColor(const glm::vec2 & texPos) const
{
	int x = texPos.x * maxCoords.x;
	int y = texPos.y * maxCoords.y;



	//auto targetPos = texPos * maxCoords;
	sf::Color color = image.getPixel(x,y);
	return glm::vec3(color.r/255.0f, color.g/255.0f, color.b/255.0f);
}
