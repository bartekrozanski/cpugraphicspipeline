#include "InstanceVertexShader.h"



InstanceVertexShader::InstanceVertexShader()
{
}

InstanceVertexShader::~InstanceVertexShader()
{
}

void InstanceVertexShader::RunShader(VertexStream & vertexStream)
{
	auto normalMatrix = glm::mat3(transpose(inverse(modelMatrix)));
	auto PVM = projectionMatrix * viewMatrix * modelMatrix;
	for (Vertex& vertex : vertexStream)
	{
		vertex.worldPos = modelMatrix * vertex.position;
		vertex.position = PVM * vertex.position;
		vertex.normal = normalMatrix * vertex.normal;
	}
}

void InstanceVertexShader::SetModelMatrix(glm::mat4 matrix)
{
	modelMatrix = matrix;
}

void InstanceVertexShader::SetViewMatrix(glm::mat4 matrix)
{
	viewMatrix = matrix;
}

void InstanceVertexShader::SetProjectionMatrix(glm::mat4 matrix)
{
	projectionMatrix = matrix;
}
