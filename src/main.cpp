#define NOMINMAX //to suppress definitions of MAX and MIN in Windows.h wihich collide with std::max
//#define BENCHMARK
#define CONSOLEWIN
#include <Windows.h>

#include <SFML/Graphics.hpp>
#include <string>
#include <iostream>
#include <vector>
#include <algorithm>

#include "Clipper.h"
#include "Framebuffer.h"
#include "Rasterizer.h"
#include "Pipeline.h"
#include "SFMLWindow.h"
#include "ConsoleWindow.h"
#include "Model.h"
#include "Instance.h"
#include "ZBuffer.h"
#include "Event.h"
#include "App.h"
#include "InputHandler.h"
#include "Actions.h"
#include "Clock.h"


int main(int argc, char **argv)
{

	Window* window;
	int screenWidth; //100;
	int screenHeight; //75;
	bool benchmark = false;
	//ConsoleWindow window(screenWidth, screenHeight);
	
	//int screenWidth = 800;
	//int screenHeight = 600;
	//SFMLWindow window(screenWidth, screenHeight);

	std::vector<std::string> args;
	args.assign(argv, argv + argc);
	if (std::find(args.begin(), args.end(), "-c") != args.end()) {
		screenWidth = 100;
		screenHeight = 75;
		window = new ConsoleWindow(screenWidth, screenHeight);
	}
	else {
		screenWidth = 800;
		screenHeight = 600;
		window = new SFMLWindow(screenWidth, screenHeight);
	}
	Rasterizer rasterizer;
	Clipper clipper;
	Framebuffer framebuffer(screenWidth, screenHeight);
	ZBuffer zBuffer(screenWidth, screenHeight);
	Pipeline pipeline(rasterizer, framebuffer, zBuffer, clipper);
	InputHandler inputHandler;
	Clock clock;
	App app(*window, pipeline);
	double milisecondsPerFrameLimit = 30.0;

	Model sphere = GenerateSphere(32, 32, 2);
	app.AddModel("cube", cube);
	app.AddModel("sphere", sphere);

	app.AddTexture("dirt", "textures/dirt.jpg");
	app.AddTexture("gold", "textures/gold.jpg");
	app.AddTexture("light", "textures/light.jpg");
	app.AddTexture("solidRed", "textures/solidRed.jpg");
	app.AddTexture("colors", "textures/colors.jpg");


	if (std::find(args.begin(), args.end(), "-b") != args.end()) {
		/* v contains x */
		benchmark = true;
		app.InstantiateModel("cube",
			"dirt",
			glm::vec3(0.0f, 2.0f, -3.0f),
			3.0f, glm::vec3(-1.0f, 0.0f, 0.5f),
			glm::vec3(1.0f, 2.0f, 1.5f)
		);

		app.InstantiateModel("cube",
			"dirt",
			glm::vec3(3.0f, 3.0f, 3.0f),
			1.0f, glm::vec3(1.0f, 0.0f, 0.0f),
			glm::vec3(1.0f, 1.0f, 1.0f)
		);
		app.InstantiateModel("cube",
			"dirt",
			glm::vec3(-1.0f, -2.0f, 0.0f),
			2.0f, glm::vec3(1.0f, 3.0f, 0.0f),
			glm::vec3(1.0f, 1.0f, 1.0f)
		);
		app.InstantiateModel("cube",
			"dirt",
			glm::vec3(-1.0f, -1.0f, 2.0f),
			4.0f, glm::vec3(1.0f, 3.0f, 0.0f),
			glm::vec3(1.0f, 1.0f, 1.0f)
		);
		app.InstantiateModel("cube",
			"dirt",
			glm::vec3(-1.0f, -2.0f, -1.0f),
			-2.0f, glm::vec3(1.0f, 3.0f, 1.0f),
			glm::vec3(2.0f, 0.2f, 0.2f)
		);
	}

	app.InstantiateModel("sphere",
		"solidRed",
		glm::vec3(0.0f, 0.0f, 0.0f),
		0.0f, glm::vec3(1.0f, 0.0f, 0.0f),
		glm::vec3(1.0f, 1.0f, 1.0f)
	);

	app.InstantiateModel("cube",
		"colors",
		glm::vec3(-1.0f, -4.0f, -0.5f),
		-2.0f, glm::vec3(1.0f, 3.0f, 1.0f),
		glm::vec3(1.5f, 1.5f, 1.5f)
	);

	app.InstantiateModel("cube",
		"colors",
		glm::vec3(3.0f, 3.0f, 0.0f),
		1.0f, glm::vec3(1.0f, 0.0f, 0.0f),
		glm::vec3(1.0f, 1.0f, 1.0f)
	);

	app.InstantiateModel("cube",
		"dirt",
		glm::vec3(-3.0f, 3.0f, 0.0f),
		2.0f, glm::vec3(1.0f, 0.0f, 0.0f),
		glm::vec3(1.5f, 1.0f, 1.0f)
	);

	app.InstantiateLight("cube",
		"light",
		glm::vec3(0.0f, 5.0f, 0.0f),
		0.0f, glm::vec3(1.0f, 0.0f, 0.0f),
		glm::vec3(0.3f, 0.3f, 0.3f),
		glm::vec3(1.0f, 1.0f, 1.0f)
);
	app.InstantiateLight("cube",
		"light",
		glm::vec3(-5.0f, 5.0f, 0.0f),
		0.0f, glm::vec3(1.0f, 0.0f, 0.0f),
		glm::vec3(0.3f, 0.3f, 0.3f),
		glm::vec3(1.0f, 1.0f, 1.0f)
	);

	app.InstantiateLight("cube",
		"light",
		glm::vec3(+5.0f, 5.0f, 0.0f),
		0.0f, glm::vec3(1.0f, 0.0f, 0.0f),
		glm::vec3(0.3f, 0.3f, 0.3f),
		glm::vec3(1.0f, 1.0f, 1.0f)
	);
	app.CreateCamera();
	double dt = 0;
	while (true)
	{
		clock.Start();


		Event event;
		while (window->PollEvent(event))
		{
			inputHandler.ProcessEvent(event);
		}
		inputHandler.ProcessPressedDownKeys();
		Action action;
		while (inputHandler.PollAction(action))
		{
			app.PerformAction(action, dt);
		}
		app.DisplayScene();
		dt = clock.Stop();
		if (!benchmark)
		{
			clock.Sleep(milisecondsPerFrameLimit - dt);
		}
		window->DisplayInTitle(clock.Stop());

	}

	return 0;
}