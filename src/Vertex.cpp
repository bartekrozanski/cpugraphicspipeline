#include "Vertex.h"

Vertex::Vertex(glm::vec3 position,
	glm::vec3 color,
	glm::vec3 normal,
	glm::vec2 texPos):
	position(position, 1.0f),
	color(color),
	normal(normal),
	texPos(texPos)
{
}

Vertex::Vertex(const Vertex & v0, const Vertex & v1, float t):
	position(glm::mix(v0.position, v1.position, t)),
	color(glm::mix(v0.color, v1.color, t)),
	normal(glm::mix(v0.normal, v1.normal, t)),
	worldPos(glm::mix(v0.worldPos, v1.worldPos, t)),
	texPos(glm::mix(v0.texPos, v1.texPos, t))
{
}

Vertex::~Vertex()
{
}
