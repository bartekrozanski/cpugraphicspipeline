#include "Viewport.h"


Viewport::Viewport(const Window & window, float x, float y, float width, float height)
{
	viewportTransform = glm::mat4(1.0f);
	viewportTransform[0][0] = ((int)(window.GetWidth() * width))/2.0f;
	viewportTransform[1][1] = ((int)(window.GetHeight() * height)) / 2.0f;
	viewportTransform[3][0] = (((int)(window.GetWidth() * width)) / 2.0f) + x;
	viewportTransform[3][1] = (((int)(window.GetHeight() * height)) / 2.0f) + y;

}

const glm::mat4& Viewport::GetViewportTransform() const
{
	return viewportTransform;
}

Viewport::~Viewport()
{
}
