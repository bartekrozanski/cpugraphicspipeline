#include "Instance.h"



Instance::Instance(Model const *model,  MyTexture const *texture) :
	scaleMatrix(glm::mat4(1.0f)),
	rotateMatrix(glm::mat4(1.0f)),
	translateMatrix(glm::mat4(1.0f)),
	dirty(true),
	model(model),
	texture(texture)
{
}


void Instance::SetAbsoluteScale(const glm::vec3& scale)
{
	scaleMatrix = glm::scale(glm::mat4(1.0f), scale);
	dirty = true;
}

void Instance::SetAbsoluteScale(float x, float y, float z)
{
	scaleMatrix = glm::scale(glm::mat4(1.0f), glm::vec3(x, y, z));
	dirty = true;
}

void Instance::SetRelativeScale(float x, float y, float z)
{
	scaleMatrix = glm::scale(scaleMatrix, glm::vec3(x, y, z));
	dirty = true;
}

void Instance::SetAbsoluteRotation(float degrees, const glm::vec3& axis)
{
	rotateMatrix = glm::rotate(glm::mat4(1.0f), degrees, axis);
	dirty = true;
}

void Instance::SetRelativeRotation(float degrees, const glm::vec3 & axis)
{
	rotateMatrix = glm::rotate(rotateMatrix, degrees, axis);
	dirty = true;
}

void Instance::SetAbsoluteTranslation(float x, float y, float z)
{
	translateMatrix = glm::translate(glm::mat4(1.0f), glm::vec3(x, y, z));
	dirty = true;
}

void Instance::SetAbsoluteTranslation(const glm::vec3 & position)
{
	translateMatrix = glm::translate(glm::mat4(1.0f), position);
	dirty = true;
}

void Instance::SetRelativeTranslation(float x, float y, float z)
{
	translateMatrix = glm::translate(translateMatrix, glm::vec3(x, y, z));
	dirty = true;
}

void Instance::SetRelativeTranslation(const glm::vec3 & position)
{
	translateMatrix = glm::translate(translateMatrix, position);
	dirty = true;
}

const glm::mat4 & Instance::GetWorldMatrix()
{
	if (dirty) modelMatrix = translateMatrix * rotateMatrix * scaleMatrix;
	return modelMatrix;
}

glm::vec3 Instance::GetPosition()
{
	return translateMatrix[3];
}

const Model *Instance::GetModel()
{
	return model;
}

const MyTexture * Instance::GetTexture()
{
	return texture;
}
