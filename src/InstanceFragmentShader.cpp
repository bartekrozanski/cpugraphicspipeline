#include "InstanceFragmentShader.h"

InstanceFragmentShader::~InstanceFragmentShader()
{
}

void InstanceFragmentShader::RunShader(FragmentStream & fragmentStream)
{

	for (Fragment& fragment : fragmentStream)
	{
		fragment.color =  texture->GetColor(fragment.texPos);
		glm::vec3 outLight(0.0f);
		/*glm::vec3 ambient(0.0f);
		glm::vec3 diffuse(0.0f);
		glm::vec3 specular(0.0f);
*/
		float ambientStrength = 0.02f;
		float diffuseStrength = 1.0f;
		float specularStrength = 1.0f;

		for (Light& light : *lights)
		{
			//ambient
			glm::vec3 lightColor = light.GetLightColor();
			outLight += lightColor * ambientStrength;

			 //diffuse 
			glm::vec3 norm = normalize(fragment.normal);
			glm::vec3 lightDir = glm::normalize(light.GetPosition() - fragment.worldPos);
			float diff = std::max(dot(norm, lightDir), 0.0f);
			outLight += diff * lightColor;


			glm::vec3 viewDir = normalize(cameraPosition - fragment.worldPos);
			glm::vec3 reflectDir = reflect(-lightDir, norm);
			float spec = pow(std::max(dot(viewDir, reflectDir), 0.0f), 32);
			outLight += specularStrength * spec * lightColor;

		}
		fragment.color = glm::clamp(
			(outLight * fragment.color), 0.f, 1.f
			);
	}
}

void InstanceFragmentShader::SetCameraPosition(glm::vec3 position)
{
	cameraPosition = position;
}

void InstanceFragmentShader::SetTexture(const MyTexture * texture)
{
	this->texture = texture;
}
