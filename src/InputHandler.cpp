#include "InputHandler.h"


InputHandler::InputHandler()
{
}


InputHandler::~InputHandler()
{
}

bool InputHandler::PollAction(Action & action)
{
	if (!actions.empty())
	{
		action = actions.back();
		actions.pop();
		return true;
	}
	return false;
}

void InputHandler::ProcessEvent(const Event & event)
{
	switch (event.type)
	{
	case EventType::KeyPressed:
		switch (event.keyEvent.key)
		{
		case Key::ArrowDown:
		case Key::ArrowLeft:
		case Key::ArrowRight:
		case Key::ArrowUp:
		case Key::A:
		case Key::W:
		case Key::S:
		case Key::D:
		case Key::J:
		case Key::K:
		case Key::L:
		case Key::I:
		case Key::U:
		case Key::O:
			SetAsPressedDown(event.keyEvent.key);
			break;
		case Key::F:
			actions.push(Action::CHANGE_DISPLAY_MODE);
			break;
		case Key::LeftSquareBracket:
			actions.push(Action::SELECT_PREVIOUS);
			break;
		case Key::RightSquareBracket:
			actions.push(Action::SELECT_NEXT);
			break;
		case Key::Esc:
			actions.push(Action::UNSELECT);
			break;
		case Key::Num1:
			actions.push(Action::SPAWN_CUBE);
			break;
		case Key::Num2:
			actions.push(Action::SPAWN_SPHERE);
			break;
		case Key::Num3:
			actions.push(Action::SPAWN_LIGHT);
			break;
		case Key::Backspace:
			actions.push(Action::DELETE_SELECTED);
			break;
		}
		break;
	case EventType::KeyReleased:
		switch (event.keyEvent.key)
		{
		case Key::ArrowDown:
		case Key::ArrowLeft:
		case Key::ArrowRight:
		case Key::ArrowUp:
		case Key::A:
		case Key::W:
		case Key::S:
		case Key::D:
		case Key::I:
		case Key::J:
		case Key::K:
		case Key::L:
		case Key::U:
		case Key::O:
			SetAsReleased(event.keyEvent.key);
			break;
		}
		break;
	default:
		break;
	}
}



void InputHandler::ProcessPressedDownKeys()
{
	for (auto it = pressedDown.begin(); it != pressedDown.end(); ++it)
	{
		switch (*it)
		{
		case Key::A:
			actions.push(Action::MOVE_CAMERA_LEFT);
			break;
		case Key::W:
			actions.push(Action::MOVE_CAMERA_FORWARD);
			break;
		case Key::S:
			actions.push(Action::MOVE_CAMERA_BACKWARD);
			break;
		case Key::D:
			actions.push(Action::MOVE_CAMERA_RIGHT);
			break;
		case Key::ArrowUp:
			actions.push(Action::ROTATE_CAMERA_UP);
			break;
		case Key::ArrowDown:
			actions.push(Action::ROTATE_CAMERA_DOWN);
			break;
		case Key::ArrowLeft:
			actions.push(Action::ROTATE_CAMERA_LEFT);
			break;
		case Key::ArrowRight:
			actions.push(Action::ROTATE_CAMERA_RIGHT);
			break;
		case Key::J:
			actions.push(Action::MOVE_SELECTED_X_NEGATIVE);
			break;
		case Key::L:
			actions.push(Action::MOVE_SELECTED_X_POSITIVE);
			break;
		case Key::I:
			actions.push(Action::MOVE_SELECTED_Y_POSITIVE);
			break;
		case Key::K:
			actions.push(Action::MOVE_SELECTED_Y_NEGATIVE);
			break;
		case Key::U:
			actions.push(Action::MOVE_SELECTED_Z_NEGATIVE);
			break;
		case Key::O:
			actions.push(Action::MOVE_SELECTED_Z_POSITIVE);
			break;
		default:
			break;
		}
	}
}

void InputHandler::SetAsPressedDown(Key key)
{
	pressedDown.insert(key);
}

void InputHandler::SetAsReleased(Key key)
{
	pressedDown.erase(key);
	//pressedDown.erase(std::remove(pressedDown.begin(), pressedDown.end(), key));
}

