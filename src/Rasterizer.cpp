#include "Rasterizer.h"



Rasterizer::Rasterizer()
{
}


Rasterizer::~Rasterizer()
{
}

FragmentStream Rasterizer::RasterizeVertexStream(const VertexStream & vertexStream, Primitive interpretation)
{
	switch (interpretation)
	{
	case Primitive::TRIANGLE:
		return RasterizeTriangles(vertexStream);
		break;
	case Primitive::FRAME:
		return RasterizeFrame(vertexStream);
		break;
	default:
		break;
	}
}


FragmentStream Rasterizer::RasterizeTriangles(const VertexStream& vertexStream)
{
	FragmentStream fragmentStream;
	for (int i = 0; i < vertexStream.size(); i += 3)
	{
		RasterizeTriangle(vertexStream[i],
			vertexStream[i + 1],
			vertexStream[i + 2],
			fragmentStream);
	}
	return fragmentStream;
}

FragmentStream Rasterizer::RasterizeFrame(const VertexStream & vertexStream)
{
	FragmentStream fragmentStream;
	for (int i = 0; i < vertexStream.size(); i += 3)
	{
		RasterizeLine(vertexStream[i],
			vertexStream[i + 1],
			fragmentStream);
		RasterizeLine(vertexStream[i+1],
			vertexStream[i+2],
			fragmentStream);
		RasterizeLine(vertexStream[i+2],
			vertexStream[i],
			fragmentStream);
	}
	return fragmentStream;
}

void Rasterizer::RasterizeTriangle(const Vertex & v0, const Vertex & v1, const Vertex & v2, FragmentStream & fragmentStream)
{
	auto v0Pos = v0.position;
	auto v1Pos = v1.position;
	auto v2Pos = v2.position;
	float xMin, xMax, yMin, yMax, w0, w1, w2, area;
	BoundingBox(v0Pos, v1Pos, v2Pos, xMin, xMax, yMin, yMax);
	area = EdgeFunction(v0Pos, v1Pos, v2Pos.x, v2Pos.y);
	for (float x = ((int)xMin) + 0.5f; x < xMax; ++x)
	{
		for (float y = ((int)yMin) + 0.5f; y < yMax; ++y)
		{
			w0 = EdgeFunction(v1Pos, v2Pos, x, y);
			w1 = EdgeFunction(v2Pos, v0Pos, x, y);
			w2 = EdgeFunction(v0Pos, v1Pos, x, y);

			if (w0 >= 0 && w1 >= 0 && w2 >= 0)
			{
				w0 /= area;
				w1 /= area;
				w2 /= area;
				fragmentStream.push_back(
					InterpolateTriangle(v0, v1, v2, x, y, w0, w1, w2)
				);
			}
		}
	}
}


void Rasterizer::RasterizeLine(const Vertex & v0, const Vertex & v1, FragmentStream & fragmentStream)
{
	int x0 = v0.position.x;
	int y0 = v0.position.y;
	int x1 = v1.position.x;
	int y1 = v1.position.y;
	if (std::abs(y1 - y0) < std::abs(x1 - x0))
	{

		if (x0 > x1)
		{
			BresenhamLineLow(v1, v0, fragmentStream);
		}
		else
		{
			BresenhamLineLow(v0, v1, fragmentStream);
		}
	}
	else
	{
		if (y0 > y1)
		{
			BresenhamLineHigh(v1, v0, fragmentStream);
		}
		else
		{
			BresenhamLineHigh(v0, v1, fragmentStream);
		}
	}
}

void Rasterizer::BresenhamLineHigh(const Vertex& v0, const Vertex& v1,
	FragmentStream& fragmentStream)
{
	std::stack<int> xStack;
	std::stack<int> yStack;

	auto x0 = v0.position.x;
	auto y0 = v0.position.y;
	auto x1 = v1.position.x;
	auto y1 = v1.position.y;

	int dx = x1 - x0;
	int dy = y1 - y0;
	int xi = 1;
	if (dx < 0)
	{
		xi = -1;
		dx = -dx;
	}
	int D = 2 * dx - dy;
	int x = x0;

	for (int y = y0; y <= y1; ++y)
	{
		xStack.push(x);
		yStack.push(y);
		if (D > 0)
		{
			x = x + xi;
			D = D - 2 * dy;
		}
		D = D + 2 * dx;
	}

	float whole = xStack.size();
	float t;
	while (!xStack.empty())
	{
		t = xStack.size() / whole;
		fragmentStream.push_back(
			InterpolateLine(v0, v1, xStack.top(), yStack.top(), t)
		);
		xStack.pop();
		yStack.pop();
	}
}

void Rasterizer::BresenhamLineLow(const Vertex& v0, const Vertex& v1,
	FragmentStream& fragmentStream)
{
	std::stack<int> xStack;
	std::stack<int> yStack;

	auto x0 = v0.position.x;
	auto y0 = v0.position.y;
	auto x1 = v1.position.x;
	auto y1 = v1.position.y;

	int dx = x1 - x0;
	int dy = y1 - y0;
	int yi = 1;
	if (dy < 0)
	{
		yi = -1;
		dy = -dy;
	}
	int D = 2 * dy - dx;
	int y = y0;

	for (int x = x0; x <= x1; ++x)
	{
		xStack.push(x);
		yStack.push(y);
		if (D > 0)
		{
			y = y + yi;
			D = D - 2 * dx;
		}
		D = D + 2 * dy;
	}

	float whole = xStack.size()+1; // TODOOOO what size exactly?>
	float t;
	while (!xStack.empty())
	{
		t = xStack.size() / whole;
		t = 0.5f;
		fragmentStream.push_back(
			InterpolateLine(v0, v1, xStack.top(), yStack.top(), t)
		);
		xStack.pop();
		yStack.pop();
	}
}

void Rasterizer::BoundingBox(const glm::vec4& v0,
	const glm::vec4& v1,
	const glm::vec4& v2,
	float & xMin, float & xMax, float & yMin, float & yMax)
{
	auto minMax = std::minmax({ v0.x, v1.x, v2.x });
	xMin = minMax.first;
	xMax = minMax.second;
	minMax = std::minmax({ v0.y, v1.y, v2.y });
	yMin = minMax.first;
	yMax = minMax.second;
}

float Rasterizer::EdgeFunction(const glm::vec4 & v0, const glm::vec4 & v1, float x, float y)
{
	return ((x - v0.x) * (v1.y - v0.y)) - ((y - v0.y)*(v1.x - v0.x));
}

float Rasterizer::Interpolate(float w0, float w1, float w2, float val0, float val1, float val2)
{
	return w0 * val0 + w1 * val1 + w2 * val2;
}

Fragment Rasterizer::InterpolateLine(const Vertex & v0,
	const Vertex & v1,
	int x, int y,
	float t)
{
	return Fragment(
		glm::vec3(x,
			y,
			glm::mix(v0.position.z, v1.position.z, t)
		),
		glm::mix(v0.color, v1.color, t),
		glm::mix(v0.worldPos, v1.worldPos, t),
		glm::mix(v0.normal, v1.normal, t),
		glm::mix(v0.texPos, v1.texPos, t)
	);
}

Fragment Rasterizer::InterpolateTriangle(const Vertex & v0,
	const Vertex & v1,
	const Vertex & v2,
	int x, int y,
	float w0,
	float w1,
	float w2)
{
	return Fragment(
		glm::vec3(
			x,
			y,
			Interpolate(w0, w1, w2, v0.position.z, v1.position.z, v2.position.z)
		),
		glm::vec3(
			Interpolate(w0, w1, w2, v0.color.r, v1.color.r, v2.color.r),
			Interpolate(w0, w1, w2, v0.color.g, v1.color.g, v2.color.g),
			Interpolate(w0, w1, w2, v0.color.b, v1.color.b, v2.color.b)
		),
		glm::vec3(
			Interpolate(w0, w1, w2, v0.worldPos.x, v1.worldPos.x, v2.worldPos.x),
			Interpolate(w0, w1, w2, v0.worldPos.y, v1.worldPos.y, v2.worldPos.y),
			Interpolate(w0, w1, w2, v0.worldPos.z, v1.worldPos.z, v2.worldPos.z)
		),
		glm::vec3(
			Interpolate(w0, w1, w2, v0.normal.x, v1.normal.x, v2.normal.x),
			Interpolate(w0, w1, w2, v0.normal.y, v1.normal.y, v2.normal.y),
			Interpolate(w0, w1, w2, v0.normal.z, v1.normal.z, v2.normal.z)
		),
		glm::vec2(
			Interpolate(w0, w1, w2, v0.texPos.x, v1.texPos.x, v2.texPos.x),
			Interpolate(w0, w1, w2, v0.texPos.y, v1.texPos.y, v2.texPos.y)
		)
	);
}
