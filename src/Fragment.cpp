#include "Fragment.h"




Fragment::Fragment(const glm::vec3 & position,
	const glm::vec3& color,
	const glm::vec3& worldPos,
	const glm::vec3& normal,
	const glm::vec2& texPos):
	position(position),
	color(color),
	worldPos(worldPos),
	normal(normal),
	texPos(texPos)
{
}

Fragment::~Fragment()
{
}
