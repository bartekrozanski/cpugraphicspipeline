#pragma once
#include "Model.h"

int GetSphereVertexIndex(int horizontalSegment, int verticalSegment, int totalHorizontal)
{
	return 1 + verticalSegment * totalHorizontal + horizontalSegment;
}

Vertex GenerateSphereVertex(glm::vec3 pos)
{
	return Vertex(
		glm::vec3(pos),
		glm::vec3(1.0f, 0.0f, 0.0f),
		glm::vec3(pos),
		glm::vec2(0.5f, 0.5f)
	);
}
Model GenerateSphere(int verticalSegments, int horizontalSegments, float radius)
{
	VertexStream vertexStream;
	Indices indices;
	vertexStream.push_back(GenerateSphereVertex(glm::vec3(0.0f, 0.0f, radius)));
	for (int vertical = 0; vertical < verticalSegments; vertical++)
	{
		for (int horizontal = 0; horizontal < horizontalSegments; ++horizontal)
		{
			float longitude = horizontal * (2 * MATH_PI / horizontalSegments);
			float latitude = (vertical+1) * (MATH_PI / (verticalSegments+2));
			float projectedSegment = radius * std::sin(latitude);
			float x = projectedSegment * std::sin(longitude);
			float y = projectedSegment * std::cos(longitude);
			float z = radius * cos(latitude);
			vertexStream.push_back(GenerateSphereVertex(glm::vec3(x, y, z)));
		}
	}
	vertexStream.push_back(GenerateSphereVertex(glm::vec3(0.0f, 0.0f, -radius)));
	for (int i = 0; i < horizontalSegments; ++i)
	{
		indices.push_back(0);
		indices.push_back(GetSphereVertexIndex(i, 1, horizontalSegments));
		indices.push_back(GetSphereVertexIndex((i+1)%horizontalSegments, 1, horizontalSegments));
	}
	for (int y = 1; y < verticalSegments-1; ++y)
	{
		for (int i = 0; i < horizontalSegments; ++i)
		{
			indices.push_back(GetSphereVertexIndex(i, y, horizontalSegments));
			indices.push_back(GetSphereVertexIndex(i, y+1, horizontalSegments));
			indices.push_back(GetSphereVertexIndex((i+1)%horizontalSegments, y+1, horizontalSegments));

			indices.push_back(GetSphereVertexIndex(i, y, horizontalSegments));
			indices.push_back(GetSphereVertexIndex((i + 1) % horizontalSegments, y + 1, horizontalSegments));
			indices.push_back(GetSphereVertexIndex((i+1)%horizontalSegments, y, horizontalSegments));
		}
	}
	for (int i = 0; i < horizontalSegments; ++i)
	{
		indices.push_back(horizontalSegments * verticalSegments + 1);
		indices.push_back(GetSphereVertexIndex((i+1)%horizontalSegments, verticalSegments-1, horizontalSegments));
		indices.push_back(GetSphereVertexIndex(i,verticalSegments-1,horizontalSegments));
	}
	return std::make_pair(vertexStream, indices);
}
