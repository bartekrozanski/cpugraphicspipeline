#include "ConsoleWindow.h"

const std::string ConsoleWindow::chars = { ' ', '`', '\'', '.', '-', ',', '_', ':', '^', '~', '!', '\"', ';', 'r', '/', '\\', '+', '(', ')', '>', '<', '|', '=', '?', 'l', 'c', 'i', '[', ']', 'v', 't', 'z', 'j', 'L', '7', 'f', 'x', '*', 's', '}', '{', 'Y', 'T', 'J', '1', 'u', 'n', 'C', 'y', 'I', 'F', 'o', '2', 'e', 'w', 'V', 'h', '3', 'k', 'a', '%', 'Z', '4', '5', 'S', 'X', '$', 'P', 'm', 'G', 'A', 'p', 'q', 'b', 'd', 'U', 'E', '&', 'K', '6', '9', 'O', 'H', 'g', '#', 'D', '8', 'R', 'Q', 'W', '0', 'M', 'B', '@', 'N' };


ConsoleWindow::ConsoleWindow(int width, int height) :
	Window(width, height)
{
	buffer = new CHAR_INFO[width * height];
	bufferBegin.X = 0;
	bufferBegin.Y = 0;

	SetWindowSize(width, height);
	SaveCurrentConsoleOutHandle();
	CreateNewScreenBuffer(width, height);
	SetActiveScreenBuffer(hNewScreenBuffer);

	CONSOLE_SCREEN_BUFFER_INFO info;
	GetConsoleScreenBufferInfo(hNewScreenBuffer, &info);
	SetWindowSize(width, height);

	auto ret = GetLargestConsoleWindowSize(hNewScreenBuffer);


	//setting up input handling
	hStdin = GetStdHandle(STD_INPUT_HANDLE);
	if (hStdin == INVALID_HANDLE_VALUE)
	{
		throw std::exception("Could not get handle to stdin");
	}
	//saving old settings to restore on object destruction
	if (!GetConsoleMode(hStdin, &oldInputMode))
	{
		throw std::exception("Could not save old input mode settings");
	}
}

ConsoleWindow::~ConsoleWindow()
{
	SetActiveScreenBuffer(hStdout);
}

void ConsoleWindow::Display(const Framebuffer& framebuffer)
{
	SMALL_RECT srctWriteRect;
	int success;

	memset(buffer, 0x0, width * height * sizeof(CHAR_INFO));

	srctWriteRect.Top = 0;    // top lt: row 10, col 0 
	srctWriteRect.Left = 0;
	srctWriteRect.Bottom = height; // bot. rt: row 11, col 79 
	srctWriteRect.Right = width;

	// Copy from the temporary buffer to the new screen buffer. 
	const Pixel* pixels = framebuffer.GetBuffer();

	for (int i = 0; i < width * height; ++i)
	{
		if (pixels[i] > 0x0)
		{
			char c;
			short color;
			PixelToConsoleOutput(c, color, pixels[i]);

			buffer[i].Char.AsciiChar = c;

			buffer[i].Attributes = color;

		}
	}

	success = WriteConsoleOutput(
		hNewScreenBuffer, // screen buffer to write to 
		buffer,        // buffer to copy from 
		bufferSize,     // col-row size of chiBuffer 
		bufferBegin,    // top left src cell in chiBuffer 
		&srctWriteRect);  // dest. screen buffer rectangle 

	if (!success)
	{
		throw std::exception("Could not write to console output");
	}
}

void ConsoleWindow::CreateNewScreenBuffer(int width, int height)
{

	hNewScreenBuffer = CreateConsoleScreenBuffer(
		GENERIC_READ |           // read/write access 
		GENERIC_WRITE,
		FILE_SHARE_READ |
		FILE_SHARE_WRITE,        // shared 
		NULL,                    // default security attributes 
		CONSOLE_TEXTMODE_BUFFER, // must be TEXTMODE 
		NULL);                   // reserved; must be NULL 

	if (hNewScreenBuffer == INVALID_HANDLE_VALUE)
	{
		throw std::exception("Could not create new screen buffer");
	}

	CONSOLE_SCREEN_BUFFER_INFO info;
	GetConsoleScreenBufferInfo(hNewScreenBuffer, &info);

	bufferSize;
	bufferSize.X = width;
	bufferSize.Y = height;
	SetConsoleScreenBufferSize(hNewScreenBuffer, bufferSize);

	info;
	GetConsoleScreenBufferInfo(hNewScreenBuffer, &info);
}

void ConsoleWindow::SaveCurrentConsoleOutHandle()
{
	hStdout = GetStdHandle(STD_OUTPUT_HANDLE);
	if (hStdout == INVALID_HANDLE_VALUE)
	{
		throw std::exception("Could not save handle to old console");
	}
}

void ConsoleWindow::SetActiveScreenBuffer(HANDLE screenBuffer)
{
	if (!SetConsoleActiveScreenBuffer(screenBuffer))
	{
		throw std::exception("Could not set active screen buffer");
	}
}

void ConsoleWindow::SetWindowSize(int width, int height)
{
	SMALL_RECT rec;
	rec.Left = 0;
	rec.Top = 0;
	rec.Bottom = height - 1;
	rec.Right = width - 1;
	SetConsoleWindowInfo(hNewScreenBuffer, TRUE, &rec);
}



void ConsoleWindow::PixelToConsoleOutput(char& c, short& color, Pixel pixel)
{
	color = 0;

	int r, g, b;
	r = pixel & 0xff;
	g = (pixel >> 8) & 0xff;
	b = (pixel >> 16) & 0xff;

	if (r > 188 || g > 188 || b > 188)
	{
		color |= FOREGROUND_INTENSITY;
	}
	color |= r > 16 ? FOREGROUND_RED : 0;
	color |= g > 16 ? FOREGROUND_GREEN : 0;
	color |= b > 16 ? FOREGROUND_BLUE : 0;

	//if (r > 0) color |= FOREGROUND_RED;
	//if (g > 0) color |= FOREGROUND_GREEN;
	//if (b > 0) color |= FOREGROUND_BLUE;

	//int rgb = r + g + b;
	//float coef = rgb / 3.0f;
	//coef = (coef / 255.0f) * (chars.size() + 1);
	//r /= 125;
	//g /= 125;
	//b /= 125;
	c = chars[(((r + g + b) / 3.0f) / 256.0f) * 94];
	//c = chars[((int)coef) % 94]; // repeair this modulo. quick workaround

	//if (r == 1) color |= FOREGROUND_INTENSITY;
	//if (g == 1) color |= FOREGROUND_INTENSITY;
	//if (b == 1) color |= FOREGROUND_INTENSITY;
	////if (r == 2) color |= FOREGROUND_RED | FOREGROUND_INTENSITY;
	////else if (r == 1) color |= FOREGROUND_RED;
	////if (g == 2) color |= FOREGROUND_GREEN| FOREGROUND_INTENSITY;
	////else if (g == 1) color |= FOREGROUND_GREEN;
	////if (b == 2) color |= FOREGROUND_BLUE| FOREGROUND_INTENSITY;
	////else if (b == 1) color |= FOREGROUND_BLUE;
}

bool ConsoleWindow::PollEvent(Event& event)
{
	INPUT_RECORD input;
	DWORD inputRead;


	PeekConsoleInput(
		hStdin,
		&input,
		1,
		&inputRead);

	if (inputRead == 1)
	{
		if (!ReadConsoleInput(
			hStdin,
			&input,
			1,
			&inputRead
		))
		{
			throw std::exception("Could not read input");
		}
		if (inputRead)
		{
			switch (input.EventType)
			{
			case KEY_EVENT:
				switch (input.Event.KeyEvent.wVirtualKeyCode)
				{
				case VK_LEFT:
					event.keyEvent.key = Key::ArrowLeft;
					break;
				case VK_RIGHT:
					event.keyEvent.key = Key::ArrowRight;
					break;
				case VK_DOWN:
					event.keyEvent.key = Key::ArrowDown;
					break;
				case VK_UP:
					event.keyEvent.key = Key::ArrowUp;
					break;
				case static_cast<int>(ConsoleKeyCodes::KEY_A):
					event.keyEvent.key = Key::A;
					break;
				case static_cast<int>(ConsoleKeyCodes::KEY_W):
					event.keyEvent.key = Key::W;
					break;
				case static_cast<int>(ConsoleKeyCodes::KEY_S):
					event.keyEvent.key = Key::S;
					break;
				case static_cast<int>(ConsoleKeyCodes::KEY_D):
					event.keyEvent.key = Key::D;
					break;
				case static_cast<int>(ConsoleKeyCodes::KEY_F) :
					event.keyEvent.key = Key::F;
					break;
				case static_cast<int>(ConsoleKeyCodes::KEY_J) :
					event.keyEvent.key = Key::J;
					break;
				case static_cast<int>(ConsoleKeyCodes::KEY_L) :
					event.keyEvent.key = Key::L;
					break;
				case static_cast<int>(ConsoleKeyCodes::KEY_I) :
					event.keyEvent.key = Key::I;
					break;
				case static_cast<int>(ConsoleKeyCodes::KEY_K) :
					event.keyEvent.key = Key::K;
					break;
				case static_cast<int>(ConsoleKeyCodes::KEY_U) :
					event.keyEvent.key = Key::U;
					break;
				case static_cast<int>(ConsoleKeyCodes::KEY_O) :
					event.keyEvent.key = Key::O;
					break;
				case static_cast<int>(ConsoleKeyCodes::KEY_LEFT_SQUARE_BRACKET) :
					event.keyEvent.key = Key::LeftSquareBracket;
					break;
				case static_cast<int>(ConsoleKeyCodes::KEY_RIGHT_SQUARE_BRACKET) :
					event.keyEvent.key = Key::RightSquareBracket;
					break;
				case VK_ESCAPE:
					event.keyEvent.key = Key::Esc;
					break;
				case static_cast<int>(ConsoleKeyCodes::Num0):
					event.keyEvent.key = Key::Num0;
					break;
				case static_cast<int>(ConsoleKeyCodes::Num1) :
					event.keyEvent.key = Key::Num1;
					break;
				case static_cast<int>(ConsoleKeyCodes::Num2) :
					event.keyEvent.key = Key::Num2;
					break;
				case static_cast<int>(ConsoleKeyCodes::Num3):
					event.keyEvent.key = Key::Num3;
					break;
				case static_cast<int>(ConsoleKeyCodes::Num4):
					event.keyEvent.key = Key::Num4;
					break;
				case static_cast<int>(ConsoleKeyCodes::Num5):
					event.keyEvent.key = Key::Num5;
					break;
				case static_cast<int>(ConsoleKeyCodes::Num6):
					event.keyEvent.key = Key::Num6;
					break;
				case static_cast<int>(ConsoleKeyCodes::Num7):
					event.keyEvent.key = Key::Num7;
					break;
				case static_cast<int>(ConsoleKeyCodes::Num8):
					event.keyEvent.key = Key::Num8;
					break;
				case static_cast<int>(ConsoleKeyCodes::Num9):
					event.keyEvent.key = Key::Num9;
					break;
				case VK_BACK:
					event.keyEvent.key = Key::Backspace;
					break;
				default:
					return false;
				}
				if (input.Event.KeyEvent.bKeyDown)
				{
					event.type = EventType::KeyPressed;
				}
				else
				{
					event.type = EventType::KeyReleased;
				}
				return true;
			default:
				break;
			}
		}
	}
	return false;
}

void ConsoleWindow::DisplayInTitle(double val)
{
	SetConsoleTitle(std::to_string(val).c_str());
}


