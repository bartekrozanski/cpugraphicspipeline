#include "Clipper.h"



Clipper::Clipper()
{
}


Clipper::~Clipper()
{
}

void Clipper::ClipTriangle(Vertex & v0, Vertex & v1, Vertex & v2)
{
	float dist0 = SignedDistanceFromPlane(v0);
	float dist1 = SignedDistanceFromPlane(v1);
	float dist2 = SignedDistanceFromPlane(v2);
	int inside = 0;
	if (dist0 > 0) inside++;
	if (dist1 > 0) inside++;
	if (dist2 > 0) inside++;


	switch (inside)
	{
	case 0:
		break;
	case 1:
		if (dist0 > 0)
		{
			ClipTriangleWithProperWindingOneInside(v0, v1, v2, dist0, dist1, dist2);
		}
		else if (dist1 > 0)
		{
			ClipTriangleWithProperWindingOneInside(v1, v2, v0, dist1, dist2, dist0);
		}
		else
		{
			ClipTriangleWithProperWindingOneInside(v2, v0, v1, dist2, dist0, dist1);
		}
		break;
	case 2:
		if (dist0 <= 0)
		{
			ClipTriangleWithProperWindingTwoInside(v2, v0, v1, dist2, dist0, dist1);
		}
		else if (dist1 <= 0)
		{
			ClipTriangleWithProperWindingTwoInside(v0, v1, v2, dist0, dist1, dist2);
		}
		else
		{
			ClipTriangleWithProperWindingTwoInside(v1, v2, v0, dist1, dist2, dist0);
		}
		break;
	case 3:
		clippedTriangles.push_back(v0);
		clippedTriangles.push_back(v1);
		clippedTriangles.push_back(v2);
		break;

	}

	

	//Vertex newVertex1, newVertex2;
	//TriangleInfo info = CreateTriangleInfo(v0, v1, v2);
	//int goodInd = 0;
	//int badInd = 0;
	//float t;
	//switch (info.insideVertices)
	//{
	//case 0:
	//	break;
	//case 1:
	//	while (!info.inside[goodInd]) goodInd++;
	//	while (!info.outside[badInd]) badInd++;
	//	clippedTriangles.push_back(*info.inside[goodInd]);
	//	t = info.distances[goodInd] / (info.distances[goodInd] - info.distances[badInd]);
	//	clippedTriangles.push_back(
	//		Vertex
	//		(*info.inside[goodInd],
	//			*info.outside[badInd],
	//			t
	//		)
	//	);
	//	badInd++;
	//	while (!info.outside[badInd]) badInd++;
	//	t = info.distances[goodInd] / (info.distances[goodInd] - info.distances[badInd]);
	//	clippedTriangles.push_back(
	//		Vertex
	//		(*info.inside[goodInd],
	//			*info.outside[badInd],
	//			t
	//		)
	//	);
	//	break;
	//case 2:
	//	while (!info.inside[goodInd]) goodInd++;
	//	while (!info.outside[badInd]) badInd++;
	//	clippedTriangles.push_back(*info.inside[goodInd]);
	//	t = info.distances[goodInd] / (info.distances[goodInd] - info.distances[badInd]);
	//	newVertex1 = Vertex(*info.inside[goodInd], *info.outside[badInd], t);
	//	clippedTriangles.push_back(newVertex1);
	//	goodInd++;
	//	while (!info.inside[goodInd]) goodInd++;
	//	clippedTriangles.push_back(*info.inside[goodInd]);
	//	clippedTriangles.push_back(*info.inside[goodInd]);
	//	t = info.distances[goodInd] / (info.distances[goodInd] - info.distances[badInd]);
	//	newVertex2 = Vertex(*info.inside[goodInd], *info.outside[badInd], t);
	//	clippedTriangles.push_back(newVertex1);
	//	clippedTriangles.push_back(newVertex2);
	//	break;

	//case 3:
	//	clippedTriangles.push_back(v0);
	//	clippedTriangles.push_back(v1);
	//	clippedTriangles.push_back(v2);
	//	break;
	//}
}

TriangleInfo Clipper::CreateTriangleInfo(Vertex & v0, Vertex & v1, Vertex & v2)
{
	TriangleInfo info = {0};
	info.distances[0] = SignedDistanceFromPlane(v0);
	info.distances[1] = SignedDistanceFromPlane(v1);
	info.distances[2] = SignedDistanceFromPlane(v2);
	if (info.distances[0] > 0)
	{
		info.inside[0] = &v0;
		info.insideVertices++;
	}
	else
	{
		info.outside[0] = &v0;
	}
	if (info.distances[1] > 0)
	{
		info.inside[1] = &v1;
		info.insideVertices++;
	}
	else
	{
		info.outside[1] = &v1;
	}
	if (info.distances[2] > 0)
	{
		info.inside[2] = &v2;
		info.insideVertices++;
	}
	else
	{
		info.outside[2] = &v2;
	}
	return info;
}

void Clipper::ClipTriangleWithProperWindingOneInside(Vertex & v0,
	Vertex & v1,
	Vertex & v2,
	float dist0,
	float dist1,
	float dist2)
{
	clippedTriangles.push_back(v0);
	clippedTriangles.push_back(
		Vertex(v0, v1, dist0 / (dist0 - dist1))
	);
	clippedTriangles.push_back(
		Vertex(v0, v2, dist0 / (dist0 - dist2))
	);
}

void Clipper::ClipTriangleWithProperWindingTwoInside(Vertex & v0, Vertex & v1, Vertex & v2, float dist0, float dist1, float dist2)
{
	Vertex newVertex1 = Vertex(v0, v1, dist0 / (dist0 - dist1));
	Vertex newVertex2 = Vertex(v2, v1, dist2 / (dist2 - dist1));
	clippedTriangles.push_back(v0);
	clippedTriangles.push_back(newVertex1);
	clippedTriangles.push_back(v2);
	clippedTriangles.push_back(v2);
	clippedTriangles.push_back(newVertex1);
	clippedTriangles.push_back(newVertex2);
}

float Clipper::SignedDistanceFromPlane(const Vertex & v)
{
	return glm::dot(v.position, normal) + offset;
}

VertexStream Clipper::ClipTriangles(VertexStream & vertexStream)
{
	clippedTriangles.clear();
	for (int i = 0; i < vertexStream.size(); i += 3)
	{
		ClipTriangle(
			vertexStream[i],
			vertexStream[i+1],
			vertexStream[i+2]
		);
	}
	return clippedTriangles;
}

void Clipper::SetClippingPlaneNormal(glm::vec4 vec)
{
	normal = glm::normalize(vec);
}

void Clipper::SetClippingPlaneOffset(float value)
{
	offset = value;
;}

