#include "App.h"
#include "sceneEditor.h"

App::App(Window & window, Pipeline & pipeline):
	window(window),
	pipeline(pipeline),
	displayMode(Primitive::TRIANGLE),
	mainViewport(Viewport(window, 0, 0, 1, 1))
{
	lights.reserve(16);
	vertexShaders[VertexShader_INSTANCE] = new InstanceVertexShader;
	fragmentShaders[FragmentShader_LIGHT] = new LightFragmentShader;
	fragmentShaders[FragmentShader_INSTANCE] = new InstanceFragmentShader(&lights);
	fragmentShaders[FragmentShader_SINGLECOLOR] = new SingleColorFragmentShader();
	sceneEditor = new SceneEditor(*this);
}

App::~App()
{
	delete sceneEditor;
	for (int i = 0; i < VertexShader_TOTAL; ++i)
	{
		delete vertexShaders[i];
	}
	for (int i = 0; i < FragmentShader_TOTAL; ++i)
	{
		delete fragmentShaders[i];
	}
}


void App::PerformAction(Action action, double dt)
{
	float amountRotation = dt * 5e-3;
	float amount = dt * 1e-1;
	glm::vec3 translation(0.0f);

	switch (action)
	{
	case CHANGE_DISPLAY_MODE:
		SwitchDisplayMode();
		break;
	case ROTATE_CAMERA_LEFT:
		RotateCamera(-amountRotation, glm::vec3(0.0f, 1.0f, 0.0f));
		break;
	case ROTATE_CAMERA_RIGHT:
		RotateCamera(amountRotation, glm::vec3(0.0f, 1.0f, 0.0f));
		break;
	case ROTATE_CAMERA_UP:
		RotateCamera(amountRotation, glm::vec3(1.0f, 0.0f, 0.0f));
		break;
	case ROTATE_CAMERA_DOWN:
		RotateCamera(-amountRotation, glm::vec3(1.0f, 0.0f, 0.0f));
		break;
	case MOVE_CAMERA_LEFT:
		MoveCamera(currentCamera->GetRight()*(amount));
		break;
	case MOVE_CAMERA_RIGHT:
		MoveCamera(currentCamera->GetRight() * (-amount));
		break;
	case MOVE_CAMERA_BACKWARD:
		MoveCamera(currentCamera->GetDirection()*(amount));
		break;
	case MOVE_CAMERA_FORWARD:
		MoveCamera(currentCamera->GetDirection()*(-amount));
		break;
	case SELECT_NEXT:
		sceneEditor->SelectNext();
		break;
	case SELECT_PREVIOUS:
		sceneEditor->SelectPrevious();
		break;
	case UNSELECT:
		sceneEditor->Unselect();
		break;
	case DELETE_SELECTED:
		sceneEditor->DeleteSelectedObject();
		break;
	case SPAWN_CUBE:
		sceneEditor->SpawnCube();
		break;
	case SPAWN_SPHERE:
		sceneEditor->SpawnSphere();
		break;
	case SPAWN_LIGHT:
		sceneEditor->SpawnLight();
		break;
	case MOVE_SELECTED_X_POSITIVE:
		sceneEditor->TranslateSelected(amount, 0.0f, 0.0f);
		break;
	case MOVE_SELECTED_X_NEGATIVE:
		sceneEditor->TranslateSelected(-amount, 0.0f, 0.0f);
		break;
	case MOVE_SELECTED_Y_POSITIVE:
		sceneEditor->TranslateSelected(0.0f, amount, 0.0f);
		break;
	case MOVE_SELECTED_Y_NEGATIVE:
		sceneEditor->TranslateSelected(0.0f, -amount, 0.0f);
		break;
	case MOVE_SELECTED_Z_POSITIVE:
		sceneEditor->TranslateSelected(0.0f, 0.0f, amount);
		break;
	case MOVE_SELECTED_Z_NEGATIVE:
		sceneEditor->TranslateSelected(0.0f, 0.0f, -amount);
		break;
	default:
		break;
	}
}

void App::DisplayScene()
{
	pipeline.Clear();
	RenderSelectedObject();
	RenderInstances();
	RenderLights();
	window.Display(pipeline.GetFramebuffer());
}


void App::AddModel(const std::string & name, Model & model)
{
	modelManager.AddModel(name, model);
}

void App::AddTexture(const std::string & name, const std::string & path)
{
	textureManager.LoadTexture(path, name);
}

void App::InstantiateModel(
	const std::string & modelName,
	const std::string& textureName,
	glm::vec3 position,
	float rotation,
	glm::vec3 rotationAxis,
	glm::vec3 scale
)
{
	instances.push_back(
		Instance(modelManager.GetModel(modelName), textureManager.GetTexture(textureName))
		);
	Instance& newInstance = instances[instances.size() - 1];
	newInstance.SetAbsoluteScale(scale);
	newInstance.SetAbsoluteRotation(rotation, rotationAxis);
	newInstance.SetAbsoluteTranslation(position);
}

void App::MoveCamera(const glm::vec3& relativePosition)
{
	currentCamera->SetRelativeTranslation(relativePosition);
}

void App::RotateCamera(float degrees, const glm::vec3 & axis)
{
	currentCamera->SetRelativeRotation(degrees, axis);
}

void App::SwitchDisplayMode()
{
	if (displayMode == Primitive::TRIANGLE) displayMode = Primitive::FRAME;
	else displayMode = Primitive::TRIANGLE;
}

void App::CreateCamera()
{
	cameras.push_back(Camera(modelManager.GetModel("cube"), textureManager.GetTexture("woodenContainer"), 45.0f, 4.0f / 3.0f, 0.01f, 100.0f));
	currentCamera = &(cameras[cameras.size() - 1]);
	currentCamera->SetAbsoluteTranslation(0.0f, 0.0f, 20.0f);

}

void App::InstantiateLight(const std::string & name,
	const std::string& textureName,
	glm::vec3 position,
	float rotation,
	glm::vec3 rotationAxis,
	glm::vec3 scale, 
	glm::vec3 lightColor)
{
	lights.push_back(Light(modelManager.GetModel(name),textureManager.GetTexture(textureName), lightColor));
	Light& newLight = lights[lights.size() - 1];
	newLight.SetAbsoluteScale(scale);
	newLight.SetAbsoluteRotation(rotation, rotationAxis);
	newLight.SetAbsoluteTranslation(position);
}

void App::RenderInstances()
{
	InstanceVertexShader* vertexShader = (InstanceVertexShader*)vertexShaders[VertexShader_INSTANCE];
	vertexShader->SetViewMatrix(currentCamera->GetViewMatrix());
	vertexShader->SetProjectionMatrix(currentCamera->GetProjectionMatrix());

	InstanceFragmentShader* fragmentShader = (InstanceFragmentShader*)fragmentShaders[FragmentShader_INSTANCE];
	fragmentShader->SetCameraPosition(currentCamera->GetPosition());

	for (Instance& instance : instances)
	{
		vertexShader->SetModelMatrix(instance.GetWorldMatrix());
		fragmentShader->SetTexture(instance.GetTexture());
		pipeline.Draw(*instance.GetModel(),
			vertexShader,
			fragmentShader,
			displayMode,
			mainViewport);
	}
}

void App::RenderLights()
{
	LightFragmentShader* fragmentShader = (LightFragmentShader*)fragmentShaders[FragmentShader_LIGHT];
	InstanceVertexShader* vertexShader = (InstanceVertexShader*)vertexShaders[VertexShader_INSTANCE];
	vertexShader->SetViewMatrix(currentCamera->GetViewMatrix());
	vertexShader->SetProjectionMatrix(currentCamera->GetProjectionMatrix());
	for (Light& light : lights)
	{
		vertexShader->SetModelMatrix(light.GetWorldMatrix());
		fragmentShader->SetShadingColor(light.GetLightColor());
		fragmentShader->SetTexture(light.GetTexture());
		pipeline.Draw(*light.GetModel(),
			vertexShader,
			fragmentShader,
			displayMode,
			mainViewport);
	}
}

void App::RenderSelectedObject()
{
	if (sceneEditor->IsSelected())
	{
		auto& selectedObject = sceneEditor->GetSelectedObject();
		SingleColorFragmentShader* fragmentShader = (SingleColorFragmentShader*)fragmentShaders[FragmentShader_SINGLECOLOR];
		InstanceVertexShader* vertexShader = (InstanceVertexShader*)vertexShaders[VertexShader_INSTANCE];
		vertexShader->SetViewMatrix(currentCamera->GetViewMatrix());
		vertexShader->SetProjectionMatrix(currentCamera->GetProjectionMatrix());
		vertexShader->SetModelMatrix(selectedObject.GetWorldMatrix());
		fragmentShader->SetShadingColor(glm::vec3(0.0f, 1.0f, 0.0f));
		pipeline.Draw(*selectedObject.GetModel(),
			vertexShader,
			fragmentShader,
			displayMode,
			mainViewport);
	}
};
