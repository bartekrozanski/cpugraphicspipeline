#include "Framebuffer.h"

Framebuffer::Framebuffer(int width, int height):
	width(width),
	height(height)
{
	buffer = new Pixel[width*height];
}



void Framebuffer::SetColor(int x, int y, Byte r, Byte g, Byte b, Byte a)
{
	Pixel pixel = (r << 0) | (g << 8) | (b << 16) | (a << 24);
	buffer[(width*height - 1) - y * width - x] = pixel;
}

void Framebuffer::SetColor(int x, int y, Pixel pixel)
{
buffer[(width*height-1) - y*width + x] = pixel;
}

void Framebuffer::Clear()
{
	std::memset(buffer, 0x0, width*height * sizeof(Pixel));
}

Framebuffer::~Framebuffer()
{
	delete[] buffer;
}
