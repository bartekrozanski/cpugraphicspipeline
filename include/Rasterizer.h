#pragma once
#include "Primitives.h"
#include "FragmentStream.h"
#include "Model.h"
#include <algorithm>
#include <stdexcept>
#include <cmath>
#include <stack>

class Rasterizer
{
public:
	Rasterizer();
	~Rasterizer();

	FragmentStream RasterizeVertexStream(const VertexStream& vertexStream,
		Primitive interpretation);

private:
	FragmentStream RasterizeTriangles(const VertexStream& vertexStream);
	FragmentStream RasterizeFrame(const VertexStream& vertexStream);


	void RasterizeTriangle(const Vertex& v0, const Vertex& v1, const Vertex& v2, FragmentStream& fragmentStream);

	void BoundingBox(const glm::vec4& v0,
		const glm::vec4& v1, 
		const glm::vec4& v2, 
		float& xMin, float& xMax, float& yMin, float &yMax);
	float EdgeFunction(const glm::vec4& v0, const glm::vec4& v1, float x, float y);
	float Interpolate(float w0, float w1, float w2, float val0, float val1, float val2);
	Fragment InterpolateTriangle(const Vertex & v0,
		const Vertex & v1,
		const Vertex & v2,
		int x, int y,
		float w0,
		float w1,
		float w2);

	Fragment InterpolateLine(const Vertex& v0, const Vertex& v1, int x, int y, float t);
	void RasterizeLine(const Vertex & v0, const Vertex & v1, FragmentStream & fragmentStream);
	void BresenhamLineLow(const Vertex& v0, const Vertex& v1,
		FragmentStream& fragmentStream);
	void BresenhamLineHigh(const Vertex& v0, const Vertex& v1,
		FragmentStream& fragmentStream);

};

