#pragma once
#include "Window.h"
#include <SFML/Graphics.hpp>
#include <string>

class SFMLWindow:
	public Window
{
public:
	SFMLWindow(int width, int height);
	virtual ~SFMLWindow() override;
	virtual void Display(const Framebuffer & framebuffer) override;
	virtual bool PollEvent(Event& event) override;
	virtual void DisplayInTitle(double val) override;


protected:
	sf::RenderWindow window;
};