#pragma once
#include "VertexStream.h"


enum VertexShaderType
{
	VertexShader_INSTANCE,
	VertexShader_TOTAL
};

class VertexShader
{
public:
	VertexShader() {};
	virtual ~VertexShader() {};
	virtual void RunShader(VertexStream& vertexStream) = 0;
};

