#pragma once

#include "Instance.h"
#include "Camera.h"
#include "Primitives.h"
#include "Viewport.h"
#include "Rasterizer.h"
#include "ZBuffer.h"
#include "FragmentShader.h"
#include "VertexShader.h"
#include "Clipper.h"


class Pipeline
{

public:
	Pipeline(Rasterizer& rasterizer,
		Framebuffer& framebuffer,
		ZBuffer& zBuffer,
		Clipper& clipper);
	~Pipeline();

	void Draw(const Model& model,
		VertexShader* vertexShader,
		FragmentShader* fragmentShader,
		Primitive interpretation,
		const Viewport& viewport);
	void Clear();
	const Framebuffer& GetFramebuffer() const { return framebuffer; }
private:
	VertexStream AssemblyPrimitives(const VertexStream& vertexStream, const Indices& indices, Primitive interpretation);
	VertexStream AssemblyTriangles(const VertexStream& vertexStream, const Indices& indices);
	VertexStream AssemblyFrame(const VertexStream& vertexStream, const Indices& indices);


	bool FacesBack(const Vertex& v0,
		const Vertex& v1,
		const Vertex& v2);

	Rasterizer& rasterizer;
	Framebuffer& framebuffer;
	ZBuffer& zBuffer;
	Clipper& clipper;
};

