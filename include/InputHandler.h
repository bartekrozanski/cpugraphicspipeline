#pragma once

#include "Event.h"
#include "Actions.h"
#include <unordered_set>
#include <queue>
#include <algorithm>

using Keys = std::unordered_set<Key>;
using Actions = std::queue<Action>;

class InputHandler
{
public:
	InputHandler();
	~InputHandler();

	bool PollAction(Action& action);

	void ProcessPressedDownKeys();
	void ProcessEvent(const Event& event);

protected:
	void SetAsPressedDown(Key key);
	void SetAsReleased(Key key);

	Keys pressedDown;
	Actions actions;
};

