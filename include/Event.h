#pragma once
enum EventType {
	KeyPressed,
	KeyReleased
};

enum Key
{
	ArrowDown,
	ArrowUp,
	ArrowLeft,
	ArrowRight,
	W,
	A,
	S,
	D,
	F,
	I,
	J,
	K,
	L,
	U,
	O,
	LeftSquareBracket,
	RightSquareBracket,
	Esc,
	Backspace,
	Num1,
	Num2,
	Num3,
	Num4,
	Num5,
	Num6,
	Num7,
	Num8,
	Num9,
	Num0,
	TOTAL,
};


class Event
{
public:

	struct KeyEvent
	{
		Key key;
	};

	Event();
	~Event();


	EventType type;
	union
	{
		KeyEvent keyEvent;
	};
};

