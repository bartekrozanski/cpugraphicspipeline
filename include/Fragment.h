#pragma once
#include <glm/mat4x4.hpp>
struct Fragment
{
public:
	Fragment(const glm::vec3& position,
		const glm::vec3& color,
		const glm::vec3& worldPos,
		const glm::vec3& normal,
		const glm::vec2& texPos);
	~Fragment();

	glm::vec3 position;
	glm::vec3 color;
	glm::vec3 worldPos;
	glm::vec3 normal;
	glm::vec2 texPos;

};

