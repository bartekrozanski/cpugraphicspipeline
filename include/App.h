#pragma once
#include "InputHandler.h"
#include "Window.h"
#include "Pipeline.h"
#include <unordered_map>
#include <string>
#include <vector>
#include "Actions.h"
#include "Light.h"
#include "LightFragmentShader.h"
#include "InstanceFragmentShader.h"
#include "InstanceVertexShader.h"
#include "SingleColorFragmentShader.h"
#include "TextureManager.h"
#include "ModelManager.h"


using Instances = std::vector<Instance>;
using Cameras = std::vector<Camera>;
using Lights = std::vector<Light>;

class SceneEditor;

class App
{
public:
	App(Window& window, Pipeline& pipeline);
	~App();


	void PerformAction(Action action, double dt);
	void DisplayScene();
	void AddModel(const std::string& name, Model& model);
	void AddTexture(const std::string& name, const std::string& path);
	void InstantiateModel(const std::string& modelName,
		const std::string& textureName,
		glm::vec3 position,
		float rotation,
		glm::vec3 rotationAxis,
		glm::vec3 scale);
	void MoveCamera(const glm::vec3& relativePosition);
	void RotateCamera(float degrees, const glm::vec3& axis);
	void SwitchDisplayMode();
	void CreateCamera();



	void InstantiateLight(const std::string& modelName,
		const std::string& textureName,
		glm::vec3 position,
		float rotation,
		glm::vec3 rotationAxis,
		glm::vec3 scale,
		glm::vec3 lightColor);


protected:

	void RenderInstances();
	void RenderLights();
	void RenderSelectedObject();

	ModelManager modelManager;
	TextureManager textureManager;
	Instances instances;
	Primitive displayMode;
	Window& window;
	Pipeline& pipeline;
	Cameras cameras;
	Camera* currentCamera;
	Lights lights;
	Viewport mainViewport;
	FragmentShader* fragmentShaders[FragmentShader_TOTAL];
	VertexShader* vertexShaders[VertexShader_TOTAL];


	SceneEditor* sceneEditor;
	friend class SceneEditor;
};

