#pragma once
#include "Vertex.h"
#include "VertexStream.h"
#include <glm/glm.hpp>



struct TriangleInfo
{
	Vertex *inside[3] = { 0 };
	Vertex *outside[3] = { 0 };
	float distances[3] = { 0 };
	int insideVertices =  0 ;
};


class Clipper
{
public:
	Clipper();
	~Clipper();


	VertexStream ClipTriangles(VertexStream& vertexStream);
	void SetClippingPlaneNormal(glm::vec4 normal);
	void SetClippingPlaneOffset(float value);

protected:


	void ClipTriangle( Vertex& v0,
		 Vertex& v1,
		 Vertex& v2
	);

	TriangleInfo CreateTriangleInfo(Vertex& v0, Vertex& v1, Vertex& v2);

	Vertex InterpolateVertex(Vertex& v0, Vertex& v1, float t);
	void ClipTriangleWithProperWindingOneInside(Vertex& v0,
		Vertex& v1,
		Vertex& v2,
		float dist0,
		float dist1,
		float dist2);
	void ClipTriangleWithProperWindingTwoInside(Vertex& v0,
		Vertex& v1,
		Vertex& v2,
		float dist0,
		float dist1,
		float dist2);

	float SignedDistanceFromPlane(const Vertex& v);
	VertexStream clippedTriangles;
	glm::vec4 normal;
	float offset;
};

