#pragma once
#include "FragmentShader.h"
#include "MyTexture.h"
class LightFragmentShader :
	public FragmentShader
{
public:
	LightFragmentShader() {};
	virtual ~LightFragmentShader() override;
	virtual void RunShader(FragmentStream & fragmentStream) override;
	void SetShadingColor(glm::vec3 color);
	void SetTexture(MyTexture const * texture);

protected:
	glm::vec3 lightColor;
	MyTexture const *texture;

};
