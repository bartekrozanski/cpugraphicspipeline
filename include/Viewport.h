#pragma once
#include "Window.h"
#include <glm/mat4x4.hpp>
class Viewport
{
public:
	Viewport(
		const Window& window,
		float x,
		float y,
		float width,
		float height
	);

	const glm::mat4& GetViewportTransform() const;

	~Viewport();

protected:
	glm::mat4 viewportTransform;
};

