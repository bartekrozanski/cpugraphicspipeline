#pragma once

#include "Framebuffer.h"
#include "Event.h"
#include "Clock.h"
#include <string>

class Window
{
public:
	Window(int width, int height);
	virtual ~Window() {};


	virtual bool PollEvent(Event& event) = 0;
	virtual void Display(const Framebuffer& framebuffer) = 0;
	int GetWidth() const { return width; }
	int GetHeight() const { return height; }
	virtual void DisplayInTitle(double val) = 0;

protected:
	int width;
	int height;
};
