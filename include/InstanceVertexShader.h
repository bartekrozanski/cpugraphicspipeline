#pragma once
#include "VertexShader.h"
#include <glm/mat4x4.hpp>
class InstanceVertexShader:
	public VertexShader
{
public:
	InstanceVertexShader();
	virtual ~InstanceVertexShader() override;

	virtual void RunShader(VertexStream& vertexStream) override;
	void SetModelMatrix(glm::mat4 matrix);
	void SetViewMatrix(glm::mat4 matrix);
	void SetProjectionMatrix(glm::mat4 matrix);


protected:
	glm::mat4 modelMatrix;
	glm::mat4 viewMatrix;
	glm::mat4 projectionMatrix;
};

