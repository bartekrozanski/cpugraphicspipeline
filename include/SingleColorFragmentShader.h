#pragma once
#include "FragmentShader.h"

class SingleColorFragmentShader:
	public FragmentShader
{
public:
	SingleColorFragmentShader() {};
	// Inherited via FragmentShader
	virtual void RunShader(FragmentStream& fragmentStream) override;

	void SetShadingColor(glm::vec3 color);
protected:
	glm::vec3 color;
};