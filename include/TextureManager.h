#include <unordered_map>
#include <string>
#include "MyTexture.h"

using Textures = std::unordered_map<std::string, MyTexture>;

class TextureManager
{
public:
	TextureManager() {};
	~TextureManager() {};

	const MyTexture * GetTexture(const std::string& name);
	void LoadTexture(const std::string& path, const std::string& name);
protected:
	Textures textures;
};