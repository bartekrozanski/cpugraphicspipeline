#pragma once
#include <utility>
#include "VertexStream.h"
#include "Indices.h"
#include <cmath>
#define MATH_PI 3.14159265358979323846f

using Model = std::pair<VertexStream, Indices>;
static Model cube(
	{
	{
			////front face
		Vertex(
			glm::vec3(1.0f, 1.0f, 1.f),
			glm::vec3(1.0f, 0.0f, 0.0f),
			glm::vec3(0.0f, 0.0f, 1.0f),
			glm::vec2(0.75f, 1.0f/3.0f * 2)
		),
		Vertex(
			glm::vec3(1.0f, -1.0f, 1.f),
			glm::vec3(1.0f, 0.0f, 0.0f),
			glm::vec3(0.0f, 0.0f, 1.0f),
			glm::vec2(0.75f, 1.0f)

			),
			Vertex(
			glm::vec3(-1.0f, -1.0f, 1.f),
			glm::vec3(1.0f,0.0f, 0.0f),
			glm::vec3(0.0f, 0.0f, 1.0f),
			glm::vec2(0.5f, 1.0f)
			),

			Vertex(
			glm::vec3(-1.0f, 1.0f, 1.f),
			glm::vec3(1.0f, 0.0f, 0.0f),
			glm::vec3(0.0f, 0.0f, 1.0f),
			glm::vec2(0.5f, 1.0f / 3.0f * 2)
			),

	//top face
	Vertex(
		glm::vec3(1.0f, 1.0f, 1.f),
		glm::vec3(0.0f, 1.0f, 0.0f),
		glm::vec3(0.0f, 1.0f, 0.0f),
		glm::vec2(0.25f, 1.0 / 3.0f * 2.0f)
		),
	Vertex(
		glm::vec3(-1.0f, 1.0f, 1.f),
		glm::vec3(0.0f, 1.0f, 0.0f),
		glm::vec3(0.0f, 1.0f, 0.0f),
		glm::vec2(0.0f, 1.0 / 3.0f * 2.0f)
		),
	Vertex(
		glm::vec3(1.0f, 1.0f, -1.f),
		glm::vec3(0.0f,1.0f, 0.0f),
		glm::vec3(0.0f, 1.0f, 0.0f),
		glm::vec2(0.25f, 1.0 / 3.0f * 1.0f)
	),
	Vertex(
		glm::vec3(-1.0f, 1.0f, -1.f),
		glm::vec3(0.0f,1.0f, 0.0f),
		glm::vec3(0.0f, 1.0f, 0.0f),
		glm::vec2(0.0f, 1.0 / 3.0f * 1.0f)
		),

	//left face
	Vertex(
		glm::vec3(-1.0f, 1.0f, 1.f),
		glm::vec3(0.0f, 0.0f, 1.0f),
		glm::vec3(-1.0f, 0.0f, 0.0f),
		glm::vec2(0.5f, 1.0f / 3.0f * 1)
		),
	Vertex(
		glm::vec3(-1.0f, -1.0f, 1.f),
		glm::vec3(0.0f, 0.0f, 1.0f),
		glm::vec3(-1.0f, 0.0f, 0.0f),
		glm::vec2(0.5f, 1.0f / 3.0f * 2)
		),
	Vertex(
		glm::vec3(-1.0f, 1.0f, -1.f),
		glm::vec3(0.0f,0.0f, 1.0f),
		glm::vec3(-1.0f, 0.0f, 0.0f),
		glm::vec2(0.25f, 1.0f / 3.0f * 1)
		),
	Vertex(
		glm::vec3(-1.0f, -1.0f, -1.f),
		glm::vec3(0.0f,0.0f, 1.0f),
		glm::vec3(-1.0f, 0.0f, 0.0f),
		glm::vec2(0.25f, 1.0f / 3.0f * 2)
		),

		//right face
		Vertex(
		glm::vec3(1.0f, 1.0f, 1.f),
		glm::vec3(0.0f, 1.0f, 1.0f),
		glm::vec3(1.0f, 0.0f, 0.0f),
		glm::vec2(0.75f, 1.0f / 3.0f * 1)
			),
		Vertex(
		glm::vec3(1.0f, 1.0f, -1.f),
		glm::vec3(0.0f, 1.0f, 1.0f),
		glm::vec3(1.0f, 0.0f, 0.0f),
		glm::vec2(1.0f, 1.0f / 3.0f * 1)
			),
		Vertex(
		glm::vec3(1.0f, -1.0f, 1.f),
		glm::vec3(0.0f,1.0f, 1.0f),
		glm::vec3(1.0f, 0.0f, 0.0f),
		glm::vec2(0.75f, 1.0f / 3.0f * 2)
			),

		Vertex(
		glm::vec3(1.0f, -1.0f, -1.f),
		glm::vec3(0.0f,1.0f, 1.0f),
		glm::vec3(1.0f, 0.0f, 0.0f),
		glm::vec2(1.0f, 1.0f / 3.0f * 2)
			),

		//back face
		Vertex(
		glm::vec3(1.0f, 1.0f, -1.f),
		glm::vec3(1.0f, 1.0f, 0.0f),
		glm::vec3(0.0f, 0.0f, -1.0f),
		glm::vec2(0.5f, 0.0f)
			),
		Vertex(
		glm::vec3(-1.0f, 1.0f, -1.f),
		glm::vec3(1.0f, 1.0f, 0.0f),
		glm::vec3(0.0f, 0.0f, -1.0f),
		glm::vec2(0.75f, 0.0f)
			),
		Vertex(
		glm::vec3(-1.0f, -1.0f, -1.f),
		glm::vec3(1.0f,1.0f, 0.0f),
		glm::vec3(0.0f, 0.0f, -1.0f),
		glm::vec2(0.75f, 1.0f / 3.0f * 1)
			),

		Vertex(
		glm::vec3(1.0f, -1.0f, -1.f),
		glm::vec3(1.0f,1.0f, 0.0f),
		glm::vec3(0.0f, 0.0f, -1.0f),
		glm::vec2(0.5f, 1.0f / 3.0f * 1)
			),
		//bottom face
		Vertex(
		glm::vec3(-1.0f, -1.0f, 1.f),
		glm::vec3(1.0f, 0.0f, 1.0f),
		glm::vec3(0.0f, -1.0f, 0.0f),
		glm::vec2(0.5f, 1.0f / 3.0f * 1)
			),
		Vertex(
		glm::vec3(1.0f, -1.0f, 1.f),
		glm::vec3(1.0f, 0.0f, 1.0f),
		glm::vec3(0.0f, -1.0f, 0.0f),
		glm::vec2(0.75f, 1.0f / 3.0f * 1)
			),
		Vertex(
		glm::vec3(1.0f, -1.0f, -1.f),
		glm::vec3(1.0f,0.0f, 1.0f),
		glm::vec3(0.0f, -1.0f, 0.0f),
		glm::vec2(0.75f, 1.0f / 3.0f * 2)
			),
		Vertex(
		glm::vec3(-1.0f, -1.0f, -1.f),
		glm::vec3(1.0f,0.0f, 1.0f),
		glm::vec3(0.0f, -1.0f, 0.0f),
		glm::vec2(0.5f, 1.0f / 3.0f * 2)
			)
	},
	{
		0,1,2,0,2,3, //front face
		4,5,6,5,7,6, //top face
		8,9,10, 9,11,10, //left face
		12,13,14,13,15,14, //right face
		16,17,18,16,18,19,//back face
		20,21,22,20,22,23 //bottom face
	}
	}
);
int GetSphereVertexIndex(int horizontalSegment, int verticalSegment,  int totalHorizontal);
Vertex GenerateSphereVertex(glm::vec3 pos);
Model GenerateSphere(int verticalSegments, int horizontalSegments, float radius);
