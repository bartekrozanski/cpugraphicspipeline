#pragma once
#include <unordered_map>
#include <string>
#include "Model.h"

using Models = std::unordered_map<std::string, Model>;

class ModelManager
{
public:
	ModelManager();
	~ModelManager();

	void AddModel(const std::string& name, const Model& model);
	const Model * GetModel(const std::string& name);

protected:
	Models models;
};

