#pragma once
#include "Instance.h"
class Light :
	public Instance
{
public:
	Light( Model const*model,  MyTexture const*consttexture, glm::vec3 lightColor);
	glm::vec3 GetLightColor() const;

protected:
	glm::vec3 lightColor;
};

