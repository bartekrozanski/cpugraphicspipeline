#pragma once
#include "Instance.h"

class Camera:
	public Instance
{
public:
	Camera( Model const* model,
		 MyTexture const* texture,
		float fov,
		float aspectRatio,
		float nearPlane,
		float farPlane);

	glm::mat4 GetViewMatrix();
	const glm::mat4& GetProjectionMatrix() const;
	glm::vec3 GetDirection() const;
	glm::vec3 GetRight() const;


protected:
	float fov;
	float aspectRatio;
	float nearPlane;
	float farPlane;
	glm::mat4 projectionMatrix;
};

