#pragma once
#include <cstring>
#include <limits>

class ZBuffer
{
public:
	ZBuffer(int width, int height);
	~ZBuffer();

	void Clear();
	bool Visible(int x, int y, float z);

protected:
	int width;
	int height;
	float *buffer;
};

