#pragma once
#include "Window.h"
#include <windows.h>
#include <iostream>
#include <stdexcept>
#include "Event.h"
#include <string>


class ConsoleWindow :
	public Window
{
public:
	ConsoleWindow(int width, int height);
	virtual ~ConsoleWindow() override;

	virtual void Display(const Framebuffer & framebuffer) override;
	virtual bool PollEvent(Event & event) override;
	virtual void DisplayInTitle(double val) override;


protected:
	void CreateNewScreenBuffer(int width, int height);
	void SaveCurrentConsoleOutHandle();
	void SetActiveScreenBuffer(HANDLE screenBuffer);
	void SetWindowSize(int width, int height);

	static void PixelToConsoleOutput(char& c, short& color, Pixel pixel);
	static const std::string chars;


	CHAR_INFO *buffer;
	HANDLE hStdout;
	HANDLE hNewScreenBuffer;
	COORD bufferSize;
	COORD bufferBegin;

	DWORD oldInputMode;
	HANDLE hStdin;


	enum class ConsoleKeyCodes
	{
		Num0 = 0x30,
		Num1,
		Num2,
		Num3,
		Num4,
		Num5,
		Num6,
		Num7,
		Num8,
		Num9,
		KEY_A = 0x41,
		KEY_D = 0x44,
		KEY_F = 0x46,
		KEY_I = 0x49,
		KEY_J = 0x4A,
		KEY_K = 0x4B,
		KEY_L = 0x4C,
		KEY_O = 0x4f,
		KEY_S = 0x53,
		KEY_U = 0x55,
		KEY_W = 0x57,
		KEY_RIGHT_SQUARE_BRACKET= 0xDD,
		KEY_LEFT_SQUARE_BRACKET= 0xDB,
	};
};

