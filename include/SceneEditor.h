#pragma once
#include "App.h"

class App;

class SceneEditor
{
public:
	SceneEditor(App& app);
	~SceneEditor() {};


	void TranslateSelected(float x, float y, float z);
	void SelectNext();
	void SelectPrevious();
	Instance& GetSelectedObject();
	void Unselect();
	bool IsSelected();


	void SpawnSphere();
	void SpawnCube();
	void SpawnLight();
	void DeleteSelectedObject();
	

protected:

	enum class SelectionType
	{
		None,
		Instance,
		Light,
		Camera,
	};

	struct Selection
	{
		SelectionType selectionType;
		int index;
	};

	void SelectFirstObject(SelectionType selectionType);
	void SelectLastObject(SelectionType selectionType);

	Selection selection;
	App& app;
};