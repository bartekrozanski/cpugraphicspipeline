#pragma once
#include <chrono>
#include <thread>

class Clock
{
public:
	Clock();
	~Clock();
	void Start();
	double Stop();
	void Sleep(double miliseconds);

protected:
	std::chrono::time_point<std::chrono::steady_clock> t0;
	std::chrono::time_point<std::chrono::steady_clock> t1;
};

