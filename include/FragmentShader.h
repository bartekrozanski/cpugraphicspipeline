#pragma once
#include "FragmentStream.h"
#include "Light.h"
#include <vector>

enum FragmentShaderType
{
	FragmentShader_LIGHT,
	FragmentShader_INSTANCE,
	FragmentShader_CAMERA,
	FragmentShader_SINGLECOLOR,
	FragmentShader_TOTAL
};


class FragmentShader
{
public:
	FragmentShader() {};
	virtual ~FragmentShader() {};
	virtual void RunShader(FragmentStream& fragmentStream) = 0;
};

