#pragma once
#include <glm/vec3.hpp>
#include <glm/vec4.hpp>
#include <glm/glm.hpp>


struct Vertex
{
public:
	Vertex() {};
	Vertex(glm::vec3 position, glm::vec3 color, glm::vec3 normal, glm::vec2 texPos);

	Vertex(const Vertex& v0, const Vertex& v1, float t);

	~Vertex();


	glm::vec4 position;
	glm::vec3 color;
	glm::vec3 normal;
	glm::vec3 worldPos;
	glm::vec2 texPos;
};

