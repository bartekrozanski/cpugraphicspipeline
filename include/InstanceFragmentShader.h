#pragma once
#include "FragmentShader.h"
#include <algorithm>
#include <glm/glm.hpp>
#include "MyTexture.h"

class InstanceFragmentShader :
	public FragmentShader
{
public:
	InstanceFragmentShader() {};
	InstanceFragmentShader(std::vector<Light>* lights) : lights(lights) {};
	virtual ~InstanceFragmentShader() override;
	virtual void RunShader(FragmentStream & fragmentStream) override;

	void SetCameraPosition(glm::vec3 position);
	void SetTexture(MyTexture const * texture);

protected:
	std::vector<Light>* lights;
	glm::vec3 cameraPosition;
	MyTexture const *texture;
};

