#pragma once
#include "Model.h"
#include <glm/mat4x4.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include "MyTexture.h"

class Instance
{
public:
	Instance(Model const *model, MyTexture const *texture);

	void SetAbsoluteScale(float x, float y, float z);
	void SetAbsoluteScale(const glm::vec3& scale);
	void SetRelativeScale(float x, float y, float z);

	void SetAbsoluteRotation(float degrees, const glm::vec3& axis);
	void SetRelativeRotation(float degrees, const glm::vec3& axis);

	void SetAbsoluteTranslation(float x, float y, float z);
	void SetAbsoluteTranslation(const glm::vec3& position);
	void SetRelativeTranslation(float x, float y, float z);
	void SetRelativeTranslation(const glm::vec3& position);

	const glm::mat4& GetWorldMatrix();
	glm::vec3 GetPosition();
	Model const * GetModel();
	MyTexture const * GetTexture();
	

protected:

	glm::mat4 scaleMatrix;
	glm::mat4 rotateMatrix;
	glm::mat4 translateMatrix;
	glm::mat4 modelMatrix;

	bool dirty;

	Model const *model;
	MyTexture const *texture;
};

