#pragma once
#include <cstring>
#include <cstdint>
using Byte = unsigned char;

using Pixel = std::uint32_t;

class Framebuffer
{
public:
	Framebuffer(int width, int height);

	void SetColor(int x, int y, Byte r, Byte g, Byte b, Byte a=0xff);
	void SetColor(int x, int y, Pixel pixel);
	const Pixel* GetBuffer() const { return buffer; }

	void Clear();
	~Framebuffer();

protected:
	int width;
	int height;
	Pixel *buffer;
};

