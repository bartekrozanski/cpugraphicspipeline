#pragma once
#include <string>
#include <SFML/Graphics/Image.hpp>
#include <glm/vec2.hpp>
#include <glm/vec3.hpp>


class MyTexture
{
public:
	MyTexture() {};
	MyTexture(const std::string& path);
	~MyTexture() {};

	glm::vec3 GetColor(const glm::vec2& texPos) const;
protected:
	sf::Image image;
	glm::vec2 maxCoords;
};