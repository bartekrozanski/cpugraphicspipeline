# What is it
This project uses solely CPU to simulate rendering pipeline found in GPU.
It supports rendering to window from SFML or to terminal using primitive ascii pixelart.

# Screenshots
<img src="screenshot1.PNG" width="400" height="300" />
<img src="screenshot2.PNG" width="400" height="300" />
<img src="screenshot3.PNG" width="400" height="300" />
<img src="screenshot4.PNG" width="400" height="300" />

# Controls
Run with -b option to turn on benchmark mode. In this mode, the framerate limit is turned off and there are more objects at the startup.

Run with -c option to display scene in terminal.

- w s a d and arrow keys -> camera movement
- 1 -> create new cube
- 2 -> create new sphere
- 3 -> create new light source
- "[" "]" (square brackets) -> object selection
- esc -> unselect object
- i j k l u o -> manipulating currently selected object
- f -> display only edges of triangles
- backspace -> delete selected object

# Building this project
Firstly - this project runs on Windows. It uses `windows.h` for terminal rendering.
1. Clone this repo
2. Download [SFML](https://www.sfml-dev.org/download/sfml/2.5.1/) (Use 64-bit version)
3. Place `SFML-2.5.1` folder in the root directory of cloned repo
4. Download [glm](https://github.com/g-truc/glm/tags)
5. Place `glm` folder in the root directory of cloned repo
6. You are ready to go! Open root directory with Visual Studio, wait for CMake to do its job and build the project
7. (Optional) From default, project will build in debug mode. Switch to release mode in Visual Studio to greatly increase framerate.
